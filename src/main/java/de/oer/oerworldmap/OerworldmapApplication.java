package de.oer.oerworldmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class OerworldmapApplication {

  public static void main(String[] args) {
    SpringApplication.run(OerworldmapApplication.class, args);
  }
}
