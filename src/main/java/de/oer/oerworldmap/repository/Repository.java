package de.oer.oerworldmap.repository;

import de.oer.oerworldmap.config.Config;

/**
 * @author fo
 */
public abstract class Repository {

  Config config;

  protected Repository(Config config) {
    this.config = config;
  }
}
