package de.oer.oerworldmap.repository;

import javax.annotation.Nonnull;

import de.oer.oerworldmap.models.Resource;

import java.io.IOException;
import java.util.List;

/**
 * @author fo
 */
public interface Readable {

  /**
   * Get a Resource specified by the given identifier.
   *
   * @param aId The identifier of the resource
   * @return The resource
   */
  Resource getResource(@Nonnull String aId) throws IOException;

  List<Resource> getAll(@Nonnull String aType) throws IOException;
}
