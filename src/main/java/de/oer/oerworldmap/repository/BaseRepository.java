package de.oer.oerworldmap.repository;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import de.oer.oerworldmap.config.Config;
import de.oer.oerworldmap.models.Commit;
import de.oer.oerworldmap.models.GraphHistory;
import de.oer.oerworldmap.models.Resource;
import de.oer.oerworldmap.models.ResourceList;
import de.oer.oerworldmap.models.TripleCommit;
import de.oer.oerworldmap.services.AccountService;
import de.oer.oerworldmap.services.IndexQueue;
import de.oer.oerworldmap.services.ResourceIndexer;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.TxnType;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.tdb2.TDB2Factory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;

@Scope("singleton")
@Component
public class BaseRepository extends Repository implements Readable, Writable, Queryable, Versionable {

  private static final Logger logger = LoggerFactory.getLogger(BaseRepository.class);

  private ElasticsearchRepository mElasticsearchRepo;
  private TriplestoreRepository mTriplestoreRepository;
  private ResourceIndexer mResourceIndexer;
  private ActorRef mIndexQueue;
  private boolean mAsyncIndexing;
  private Dataset dataset;

  @Autowired
  public BaseRepository(Config conf, AccountService accountService) throws IOException {
    this(conf, new ElasticsearchRepository(conf), accountService);
  }

  public BaseRepository(final Config aConfiguration,
                        final ElasticsearchRepository aElasticsearchRepo,
                        final AccountService aAccountService) throws IOException {

    super(aConfiguration);
    logger.info("Creating BaseRepository");
    if (aElasticsearchRepo == null) {
      mElasticsearchRepo = new ElasticsearchRepository(config);
    } else {
      mElasticsearchRepo = aElasticsearchRepo;
    }
    dataset = TDB2Factory.connectDataset(config.tdb().dir());

    File commitDir = new File(config.graph().history().dir());
    if (!commitDir.exists()) {
      logger.warn("Commit dir does not exist");
      if (!commitDir.mkdir()) {
        throw new IOException("Could not create commit dir");
      }
    }

    File historyFile = new File(config.graph().history().file());
    if (!historyFile.exists()) {
      logger.warn("History file does not exist");
      if (!historyFile.createNewFile()) {
        throw new IOException("Could not create history file");
      }
    }
    GraphHistory graphHistory = new GraphHistory(commitDir, historyFile);

    Model mDb = dataset.getDefaultModel();
    mResourceIndexer = new ResourceIndexer(mDb, mElasticsearchRepo, graphHistory, aAccountService,
      config.jsonld().context());

    //Start write transaction to load commits
    dataset.begin(TxnType.WRITE);
    try {
      if (mDb.isEmpty() && config.graph().history().autoload()) {
        logger.info("Loading commit history to triple store");
        List<Commit> commits = graphHistory.log();
        ListIterator<Commit> listIterator = commits.listIterator(commits.size());
        int count = 0;
        while (listIterator.hasPrevious()) {
          count += 1;
          if (count % 100 == 0) {
            logger.info("Loading commit " + count + "/" + commits.size());
          }
          listIterator.previous().getDiff().apply(mDb);
        }
        logger.info("Loaded commit history to triple store. Starting indexing");
        mResourceIndexer.index("*");
        logger.info("Indexed all resources from triple store");
      }
      mIndexQueue = ActorSystem.create().actorOf(IndexQueue.props(mResourceIndexer));
      mTriplestoreRepository = new TriplestoreRepository(config, mDb, graphHistory);

      mAsyncIndexing = config.index().async();
    } finally {
      dataset.commit();
      dataset.end();
    }
  }

  @Override
  public Resource deleteResource(@Nonnull String aId, Map<String, String> aMetadata)
    throws IOException {

    Resource resource;
    Commit.Diff diff = null;
    dataset.begin(TxnType.WRITE);
    try {
      resource = mTriplestoreRepository.deleteResource(aId, aMetadata);
      if (resource != null) {
        Resource result = mElasticsearchRepo.deleteResource(resource, aMetadata);
        diff = mTriplestoreRepository.getDiff(resource).reverse();
      }
    } finally {
      dataset.commit();
      dataset.end();
    }
    if (diff != null) {
      index(diff);
    }
    return resource;
  }

  @Override
  public void addResource(@Nonnull Resource aResource, Map<String, String> aMetadata)
    throws IOException {
    Commit.Diff diff;
    dataset.begin(TxnType.WRITE);
    try {
      diff = mTriplestoreRepository.getDiff(aResource);
      TripleCommit.Header header = new TripleCommit.Header(
        aMetadata.get(TripleCommit.Header.AUTHOR_HEADER),
        ZonedDateTime.parse(aMetadata.get(TripleCommit.Header.DATE_HEADER)),
        aResource.getId());

      Commit commit = new TripleCommit(header, diff);

      mTriplestoreRepository.commit(commit);
    } finally {
      dataset.commit();
      dataset.end();
    }
    index(diff);
  }

  /**
   * Add several CBDs of resources, using individual commits with metadata provided in Map
   *
   * @param aResources The resources to be added
   * @param aMetadata The metadata to use
   */
  @Override
  public void addResources(@Nonnull List<Resource> aResources, Map<String, String> aMetadata)
    throws IOException {
    Commit.Diff indexDiff;
    dataset.begin(TxnType.WRITE);
    try {
      List<Commit> commits = new ArrayList<>();
      indexDiff = new TripleCommit.Diff();
      for (Resource resource : aResources) {
        TripleCommit.Header header = new TripleCommit.Header(
          aMetadata.get(TripleCommit.Header.AUTHOR_HEADER),
          ZonedDateTime.parse(aMetadata.get(TripleCommit.Header.DATE_HEADER)),
          resource.getId());
        Commit.Diff diff = mTriplestoreRepository.getDiff(resource);
        indexDiff.append(diff);
        commits.add(new TripleCommit(header, diff));
      }

      mTriplestoreRepository.commit(commits);
    } finally {
      dataset.commit();
      dataset.end();
    }
    index(indexDiff);
  }

  /**
   * Import resources, extracting any embedded resources and adding those too, in the same commit
   *
   * @param aResources The resources to flatten and import
   */
  public void importResources(@Nonnull List<Resource> aResources, Map<String, String> aMetadata)
    throws IOException {

    Commit.Diff diff;

    dataset.begin(TxnType.WRITE);
    try {
      diff = mTriplestoreRepository.getDiffs(aResources);

      TripleCommit.Header header = new TripleCommit.Header(
        aMetadata.get(TripleCommit.Header.AUTHOR_HEADER),
        ZonedDateTime.parse(aMetadata.get(TripleCommit.Header.DATE_HEADER)),
        true);
      mTriplestoreRepository.commit(new TripleCommit(header, diff));
    } finally {
      dataset.commit();
      dataset.end();
    }
    index(diff);
  }

  @Override
  public ResourceList query(@Nonnull String aQueryString, int aFrom, int aSize, String aSortOrder,
    Map<String, List<String>> aFilters) {
    return query(aQueryString, aFrom, aSize, aSortOrder, aFilters, null);
  }

  public ResourceList query(@Nonnull String aQueryString, int aFrom, int aSize, String aSortOrder,
    Map<String, List<String>> aFilters, QueryContext aQueryContext) {
    ResourceList resourceList;
    try {
      resourceList = mElasticsearchRepo
        .query(aQueryString, aFrom, aSize, aSortOrder, aFilters, aQueryContext);
    } catch (IOException e) {
      logger.error("Could not query Elasticsearch repository", e);
      return null;
    }
    return resourceList;
  }

  public JsonNode reconcile(@Nonnull String aQueryString, int aFrom, int aSize, String aSortOrder,
    Map<String, List<String>> aFilters, QueryContext aQueryContext,
    final Locale aPreferredLocale) throws IOException {
    return mElasticsearchRepo
      .reconcile(aQueryString, aFrom, aSize, aSortOrder, aFilters, aQueryContext, aPreferredLocale);
  }

  @Override
  public Resource getResource(@Nonnull String aId) {
    return getResource(aId, null);
  }

  @Override
  public Resource getResource(@Nonnull String aId, String aVersion) {
    dataset.begin(TxnType.READ);
    try {
      Resource result = mTriplestoreRepository.getResource(aId, aVersion);
      dataset.commit();
      return result;
    } finally {
      dataset.end();
    }
  }

  public boolean hasResource(String aId) {
    dataset.begin(TxnType.READ);
    try {
      boolean result = mTriplestoreRepository.hasResource(aId);
      dataset.commit();
      return result;
    } finally {
      dataset.end();
    }
  }

  public List<Resource> getResources(@Nonnull String aField, @Nonnull Object aValue) {
    return mElasticsearchRepo.getResources(aField, aValue);
  }

  @Override
  public List<Resource> getAll(@Nonnull String aType) {
    List<Resource> resources = new ArrayList<>();
    try {
      resources = mElasticsearchRepo.getAll(aType);
    } catch (IOException e) {
      logger.error("Could not query Elasticsearch repository", e);
    }
    return resources;
  }

  @Override
  public Resource stage(Resource aResource) throws IOException {
    dataset.begin(TxnType.READ);
    try {
      Resource result = mTriplestoreRepository.stage(aResource);
      dataset.commit();
      return result;
    } finally {
      dataset.end();
    }
  }

  @Override
  public List<Commit> log(String aId) {
    return mTriplestoreRepository.log(aId);
  }

  @Override
  public void commit(Commit aCommit) throws IOException {
    dataset.begin(TxnType.WRITE);
    try {
      mTriplestoreRepository.commit(aCommit);
    } finally {
      dataset.commit();
      dataset.end();
    }
  }

  @Override
  public Commit.Diff getDiff(Resource aResource) {
    dataset.begin(TxnType.READ);
    try {
      Commit.Diff result = mTriplestoreRepository.getDiff(aResource);
      dataset.commit();
      return result;
    } finally {
      dataset.end();
    }
  }

  @Override
  public Commit.Diff getDiff(List<Resource> aResources) {
    dataset.begin(TxnType.READ);
    try {
      Commit.Diff result = mTriplestoreRepository.getDiff(aResources);
      dataset.commit();
      return result;
    } finally {
      dataset.end();
    }
  }

  public void index(String aId) {
    dataset.begin(TxnType.WRITE);
    try {
      if (mAsyncIndexing) {
        mIndexQueue.tell(aId, mIndexQueue);
      } else {
        mResourceIndexer.index(aId);
      }
    } finally {
      dataset.commit();
      dataset.end();
    }
  }

  public String sparql(String q) {
    dataset.begin(TxnType.READ);
    try {
      String result = mTriplestoreRepository.sparql(q);
      dataset.commit();
      return result;
    } finally {
      dataset.end();
    }
  }

  public String update(String delete, String insert, String where) {
    Commit.Diff diff = mTriplestoreRepository.update(delete, insert, where);
    return diff.toString();
  }

  public String label(String aId) {
    dataset.begin(TxnType.READ);
    try {
      String result = mTriplestoreRepository.label(aId);
      dataset.commit();
      return result;
    } finally {
      dataset.end();
    }
  }

  public void index(Commit.Diff aDiff) {
    dataset.begin(TxnType.WRITE);
    try {
      if (mAsyncIndexing) {
        mIndexQueue.tell(aDiff, mIndexQueue);
      } else {
        mResourceIndexer.index(aDiff);
      }
    } finally {
      dataset.commit();
      dataset.end();
    }
  }
}
