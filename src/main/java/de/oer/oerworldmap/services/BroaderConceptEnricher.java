package de.oer.oerworldmap.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.apache.jena.atlas.RuntimeIOException;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.shared.Lock;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author fo, pvb
 */
public class BroaderConceptEnricher implements ResourceEnricher {

  private static final ObjectMapper mObjectMapper = new ObjectMapper();

  private final Model mConceptSchemes;

  private static final Property mBroader = ResourceFactory
    .createProperty("http://www.w3.org/2004/02/skos/core#broader");

  private static final String SELECT_BROADER =
    "SELECT ?broader WHERE {" +
      "  <%1$s> <" + mBroader + ">+ ?broader " +
      "}";

  public BroaderConceptEnricher() {

    this.mConceptSchemes = ModelFactory.createDefaultModel();

    try {
      // Load ESC
      RDFDataMgr.read(
        mConceptSchemes,
        fixContext(getResourceAsStream("public/json/esc.json")),
        Lang.JSONLD);

      // Load ISCED-1997
      RDFDataMgr.read(
        mConceptSchemes,
        fixContext(getResourceAsStream("public/json/isced-1997.json")),
        Lang.JSONLD);
    } catch (IOException e) {
      throw new RuntimeIOException(e);
    }
  }

  private static InputStream getResourceAsStream(String path) throws IOException {
    InputStream inputStream = Thread.currentThread().getContextClassLoader()
      .getResourceAsStream(path);
    if (inputStream == null) {
      inputStream = new FileInputStream(path);
    }
    return inputStream;
  }

  private static InputStream fixContext(InputStream resourceInputStream) throws IOException {
    byte[] resourceBytes = resourceInputStream.readAllBytes();
    JsonNode resourceJson = mObjectMapper.readTree(resourceBytes);
    JsonNode contextIri = resourceJson.get("@context");
    if (contextIri == null || !contextIri.textValue().equals("https://oerworldmap.org/assets/json/context.json")) {
      return new ByteArrayInputStream(resourceBytes);
    }
    InputStream contextInputStream = getResourceAsStream("public/json/context.json");
    JsonNode contextJson = mObjectMapper.readTree(contextInputStream);
    ((ObjectNode)resourceJson).set("@context", contextJson.get("@context"));
    return new ByteArrayInputStream(mObjectMapper.writeValueAsBytes(resourceJson));
  }

  public void enrich(Model aToBeEnriched) {

    Model broaderConcepts = ModelFactory.createDefaultModel();

    aToBeEnriched.enterCriticalSection(Lock.READ);
    try {
      for (Statement stmt : aToBeEnriched.listStatements().toSet()) {
        if (stmt.getObject().isResource()) {
          try (QueryExecution queryExecution = QueryExecutionFactory.create(
            String.format(SELECT_BROADER, stmt.getObject()), mConceptSchemes)) {
            ResultSet resultSet = queryExecution.execSelect();
            while (resultSet.hasNext()) {
              QuerySolution querySolution = resultSet.next();
              if (!stmt.getPredicate().equals(mBroader)) {
                Statement broaderConcept = ResourceFactory
                  .createStatement(stmt.getSubject(), stmt.getPredicate(),
                    querySolution.get("broader").asResource());
                broaderConcepts.add(broaderConcept);
              }
            }
          }
        }
      }
      aToBeEnriched.add(broaderConcepts);
    } finally {
      aToBeEnriched.leaveCriticalSection();
    }
  }
}
