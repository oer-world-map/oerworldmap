package de.oer.oerworldmap.services;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigRenderOptions;
import com.typesafe.config.ConfigValue;

import de.oer.oerworldmap.helpers.FileHelpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author pvb
 */
public abstract class AbstractConfig {
  private static final Logger logger = LoggerFactory.getLogger(AbstractConfig.class);

  protected Set<Map.Entry<String, ConfigValue>> mEntries;
  protected Config mConfig;

  protected AbstractConfig(String aConfigFile, String aFileType) {
    File configFile = new File(aConfigFile);
    try {
      FileHelpers.checkFileExists(configFile, aFileType);
    } catch (FileNotFoundException e) {
      logger.error("Could not load file", e);
    }
    mConfig = ConfigFactory.parseFile(configFile).resolve();
    mEntries = mConfig.entrySet();
  }

  private Map<String, Double> getBoosts() {
    Map<String, Double> result = new HashMap<>();
    for (Map.Entry<String, ConfigValue> entry : mEntries) {
      if (entry.getKey().startsWith("\"boost:")) {
        String key = entry.getKey().replaceAll("\"", "").substring(6);
        Double value = Double.valueOf((entry.getValue()).render(ConfigRenderOptions.defaults()));
        result.put(key, value);
      }
    }
    return result;
  }

  public String[] getBoostsForElasticsearch() {
    List<String> result = new ArrayList<>();
    Map<String, Double> boostMap = getBoosts();
    for (Map.Entry<String, Double> boost : boostMap.entrySet()) {
      result.add(boost.getKey() + "^" + boost.getValue());
    }
    return result.toArray(new String[0]);
  }
}
