package de.oer.oerworldmap.services.exporter;

import de.oer.oerworldmap.models.Resource;
import de.oer.oerworldmap.models.ResourceList;

/**
 * Created by fo on 13.10.16.
 */
public interface Exporter {

  String export(Resource aResource);

  String export(ResourceList aResourceList);
}
