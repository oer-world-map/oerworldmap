package de.oer.oerworldmap.services.exporter;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.oer.oerworldmap.models.MetadataRecord;
import de.oer.oerworldmap.models.Resource;
import de.oer.oerworldmap.models.ResourceList;

import java.util.Collections;

/**
 * Created by fo on 27.07.17.
 */
public class JsonSchemaExporter implements Exporter {

  @Override
  public String export(Resource aResource) {
    return export(new ResourceList(Collections.singletonList(aResource), 0, "", 0, 0, null, null));
  }

  @Override
  public String export(ResourceList aResourceList) {

    ObjectNode schema = new ObjectNode(JsonNodeFactory.instance);
    ObjectNode properties = new ObjectNode(JsonNodeFactory.instance);
    ObjectNode idProperty = new ObjectNode(JsonNodeFactory.instance);
    ArrayNode items = new ArrayNode(JsonNodeFactory.instance);
    ArrayNode suggest = new ArrayNode(JsonNodeFactory.instance);

    for (Resource item : aResourceList.getItems()) {
      items.add(item.getAsResource(MetadataRecord.RESOURCE_KEY).getId());
      suggest.add(item.getAsResource(MetadataRecord.RESOURCE_KEY).toJson());
    }

    idProperty.put("type", "string");
    if (aResourceList.getItems().size() > 0) {
      idProperty.set("enum", items);
    } else {
      idProperty.put("pattern", "(?!)");
    }

    properties.set("@id", idProperty);

    schema.put("type", "object");
    schema.set("properties", properties);
    schema.set("_suggest", suggest);

    return schema.toString();
  }
}
