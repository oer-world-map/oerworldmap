package de.oer.oerworldmap.services;

public interface AccountService {
  boolean deleteUser(String username);
  String getProfileId(String username);
  void setProfileId(String username, String profileId);
  String getUsername(String profileId);
}
