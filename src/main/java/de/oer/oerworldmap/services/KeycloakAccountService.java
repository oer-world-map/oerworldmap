package de.oer.oerworldmap.services;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.UserRepresentation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

/**
 * @author fo
 */
public class KeycloakAccountService implements AccountService {
  private static final Logger logger = LoggerFactory.getLogger(KeycloakAccountService.class);

  private RealmResource mRealm;

  public KeycloakAccountService(String aServerUrl, String aRealm, String aUsername, String aPassword, String aClientId) {
    mRealm = Keycloak.getInstance(aServerUrl, "master", aUsername, aPassword, aClientId).realm(aRealm);
  }

  public boolean deleteUser(String username) {
    UserRepresentation user = getUser(username);
    if (user != null) {
      return mRealm.users().delete(user.getId()).getStatus() == 204;
    }
    return false;
  }

  public String getProfileId(String username) {
    UserRepresentation user = getUser(username);
    if (user != null && user.getAttributes() != null && user.getAttributes().containsKey("profile_id")) {
      return user.getAttributes().get("profile_id").get(0);
    }
    return null;
  }

  public void setProfileId(String username, String profileId) {
    UserRepresentation user = getUser(username);
    if (user != null) {
      if (user.getAttributes() != null) {
        user.getAttributes().put("profile_id", Collections.singletonList(profileId));
      } else {
        Map<String, List<String>> attributes = new HashMap<>();
        attributes.put("profile_id", Collections.singletonList(profileId));
        user.setAttributes(attributes);
      }
      mRealm.users().get(user.getId()).update(user);
    }
  }

  public String getUsername(String profileId) {
    UserRepresentation user = mRealm.users().list(0, Integer.MAX_VALUE).stream()
      .filter(u -> u.getAttributes() != null && u.getAttributes().containsKey("profile_id") && u.getAttributes().get("profile_id").contains(profileId))
      .findFirst().orElse(null);
    return user != null ? user.getUsername() : null;
  }

  public String getUserAttribute(String username, String attribute) {
    UserRepresentation user = getUser(username);
    if (user == null) {
      return null;
    }
    return user.firstAttribute(attribute);
  }

  private UserRepresentation getUser(String username) {
    return mRealm.users().search(username).stream()
      .filter(u -> u.getUsername().equals(username)).findFirst().orElse(null);
  }

  public boolean sendPasswordReset(String username) {
    UserRepresentation user = getUser(username);
    if (user != null) {
      // https://www.keycloak.org/docs-api/24.0.5/javadocs/org/keycloak/admin/client/resource/UserResource.html#executeActionsEmail(java.lang.String,java.lang.String,java.lang.Integer,java.util.List)
      // 7 days = 604800 seconds
      mRealm.users().get(user.getId()).executeActionsEmail("oerworldmap", "/resource", 604800, Arrays.asList("UPDATE_PASSWORD", "TERMS_AND_CONDITIONS"));
      // the function always returns void
      return true;
    }
    return false;
  }

  public boolean isUserActive(String username) {
    return mRealm.users().search(username).stream()
      .filter(u -> u.getUsername().equals(username))
      .filter(u -> u.isEnabled())
      .findFirst()
      .orElse(null) != null;
  }
}
