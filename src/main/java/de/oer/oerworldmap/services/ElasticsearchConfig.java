package de.oer.oerworldmap.services;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.oer.oerworldmap.config.Config;
import de.oer.oerworldmap.helpers.UniversalFunctions;

public class ElasticsearchConfig {

  private Logger logger = LoggerFactory.getLogger(ElasticsearchConfig.class);

  private static final String INDEX_CONFIG_FILE = "conf/index-config.json";

  private RestHighLevelClient mEsClient;

  private Config config;

  // HOST
  private String server;
  private Integer port;

  // CLIENT
  private String indexName;
  private String indexType;
  private String clusterName;
  private Map<String, String> mIndexSettings;
  private Map<String, String> mClusterSettings;
  private WriteRequest.RefreshPolicy mRefreshPolicy;


  public WriteRequest.RefreshPolicy getRefreshPolicy() {
    return mRefreshPolicy;
  }

  public ElasticsearchConfig(Config config) {
    this.config = config;

    // HOST
    server = config.es().hostServer();
    port = config.es().hostPortHttp();

    indexName = config.es().indexName();
    indexType = config.es().indexType();
    clusterName = config.es().clusterName();

    final RestClientBuilder builder = RestClient.builder(
      new HttpHost(server, port, "http"));
    mEsClient = new RestHighLevelClient(builder);

    if (!indexExists(indexName)) {
      CreateIndexResponse response = null;
      try {
        response = createIndex(indexName);
      } catch (IOException e) {
        logger.error("Failing to create index '".concat(indexName).concat("', caused by: "), e);
      }
      if (response.isAcknowledged()) {
        logger.info("Created index \"" + indexName + "\".");
      }
    }

    // INDEX SETTINGS
    mIndexSettings = new HashMap<>();
    mIndexSettings.put("index.name", indexName);
    mIndexSettings.put("index.type", indexType);

    // CLUSTER SETTINGS
    mClusterSettings = new HashMap<>();
    mClusterSettings.put("cluster.name", clusterName);

    switch (config.es().requestRefreshpolicy()) {
      case ("IMMEDIATE"):
        mRefreshPolicy = WriteRequest.RefreshPolicy.IMMEDIATE;
        break;
      case ("WAIT_UNTIL"):
        mRefreshPolicy = WriteRequest.RefreshPolicy.WAIT_UNTIL;
        break;
      default:
        mRefreshPolicy = WriteRequest.RefreshPolicy.NONE;
    }
  }

  public boolean indexExists(String index) {
    GetIndexRequest request = new GetIndexRequest().indices(index);
    // TODO with ES v 6.3: return mEsClient.indices().exists(request);
    CloseableHttpClient httpClient = HttpClients.createDefault();
    HttpHead head = new HttpHead(
      "http://".concat(this.server).concat(":").concat(this.port.toString()).concat("/")
        .concat(index));
    try {
      final CloseableHttpResponse response = httpClient.execute(head);
      return (response.getStatusLine().getStatusCode() == 200);
    } catch (IOException e) {
      logger.error("Could not query for index", e);
    }
    return false;
  }

  public CreateIndexResponse createIndex(String index) throws IOException {
    CreateIndexRequest request = new CreateIndexRequest(index);
    // TODO : to be configured (optional)
    /*request.settings(Settings.builder()
      .put("index.number_of_shards", 3)
      .put("index.number_of_replicas", 2)
    );*/
    request.source(getIndexConfigString(), XContentType.JSON);
    return mEsClient.indices().create(request, RequestOptions.DEFAULT);
  }

  public AcknowledgedResponse deleteIndex(String index) throws IOException {
    DeleteIndexRequest request = new DeleteIndexRequest(index);
    return mEsClient.indices().delete(request, RequestOptions.DEFAULT);
  }

  public void tearDown() throws Exception {
    if (mEsClient != null) {
      mEsClient.close();
    }
  }

  public Fuzziness getFuzziness() {
    String fuzzyString = config.es().searchFuzzyness();
    if (StringUtils.isEmpty(fuzzyString) || fuzzyString.equals("AUTO")) {
      return Fuzziness.AUTO;
    }
    if (fuzzyString.equals("ZERO")) {
      return Fuzziness.ZERO;
    }
    if (fuzzyString.equals("ONE")) {
      return Fuzziness.ONE;
    }
    if (fuzzyString.equals("TWO")) {
      return Fuzziness.TWO;
    }
    return Fuzziness.AUTO;
  }

  public String getIndex() {
    return mIndexSettings.get("index.name");
  }

  public String getType() {
    return indexType;
  }

  public String getCluster() {
    return clusterName;
  }

  public String getServer() {
    return server;
  }

  public String toString() {
    return config.toString();
  }

  public String getIndexConfigString() throws IOException {
    return UniversalFunctions.readFile(INDEX_CONFIG_FILE, StandardCharsets.UTF_8);
  }

  public Map<String, String> getClusterSettings() {
    return mClusterSettings;
  }

  public RestHighLevelClient getClient() {
    return mEsClient;
  }
}
