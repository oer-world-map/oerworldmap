package de.oer.oerworldmap.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

import de.oer.oerworldmap.config.Config;
import de.oer.oerworldmap.config.Config.KeycloakCfg;

/**
 * FactoryBean creating an AccountService.
 *
 * If a keycloak config is present the service will use a keycloak connection,
 * else the returned instance will be a MemoryAccountService
 */
public class AccountServiceFactory implements FactoryBean<AccountService> {

  @Autowired
  private Config conf;

  private static final Logger logger = LoggerFactory.getLogger(AccountServiceFactory.class);

  @Override
  public AccountService getObject() throws Exception {
    AccountService accountService;
    KeycloakCfg keycloakConfig = conf.keycloak();
    if (keycloakConfig != null) {
      logger.info("Initializing Keycloak AccountService");
      accountService = new KeycloakAccountService(
          keycloakConfig.serverUrl(),
          keycloakConfig.realm(),
          keycloakConfig.username(),
          keycloakConfig.password(),
          keycloakConfig.client());
    } else {
      logger.info("Initializing Memory AccountService");
      accountService = new MemoryAccountService();
    }
    return accountService;
  }

  @Override
  public Class<?> getObjectType() {
    return AccountService.class;
  }

  @Override
  public boolean isSingleton() {
    return true;
  }
}
