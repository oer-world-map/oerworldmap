package de.oer.oerworldmap.controllers;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class CachedInterceptor implements HandlerInterceptor {

  private static String currentEtag = UUID.randomUUID().toString();
  private static final String CACHE_CONTROL = "Cache-Control";
  private static final String ETAG = "Etag";
  private static final String IF_NONE_MATCH = "If-None-Match";

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    String ifNoneMatch = request.getHeader(IF_NONE_MATCH);
    if (!StringUtils.isEmpty(ifNoneMatch) && currentEtag.equals(ifNoneMatch)) {
      response.setStatus(HttpStatus.NOT_MODIFIED.value());
      return false;
    }
    response.setHeader(CACHE_CONTROL, "private");
    response.setHeader(ETAG, currentEtag);
    return HandlerInterceptor.super.preHandle(request, response, handler);
  }

  public static void updateEtag() {
    currentEtag = UUID.randomUUID().toString();
  }
}
