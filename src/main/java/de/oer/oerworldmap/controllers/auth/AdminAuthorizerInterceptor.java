package de.oer.oerworldmap.controllers.auth;

import java.util.List;

import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;

/**
 * Authorizer requiring the admin role for all methods
 */
@Component
public class AdminAuthorizerInterceptor implements AuthorizerInterceptor {
  @Override
  public boolean authorizeGet(HttpServletRequest request, List<String> roles) {
    return hasAdminRole(roles);
  }
  @Override
  public boolean authorizePost(HttpServletRequest request, List<String> roles) {
    return hasAdminRole(roles);
  }
  @Override
  public boolean authorizePut(HttpServletRequest request, List<String> roles) {
    return hasAdminRole(roles);
  }
  @Override
  public boolean authorizeDelete(HttpServletRequest request, List<String> roles) {
    return hasAdminRole(roles);
  }
}
