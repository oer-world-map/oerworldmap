package de.oer.oerworldmap.controllers.auth;

import java.util.List;

import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;

@Component
public class LogAuthorizerInterceptor implements AuthorizerInterceptor {

  @Override
  public boolean authorizeGet(HttpServletRequest request, List<String> roles) {
    return isEditorOrAdmin(roles);
  }

  @Override
  public boolean authorizePost(HttpServletRequest request, List<String> roles) {
    return isEditorOrAdmin(roles);
  }

  @Override
  public boolean authorizePut(HttpServletRequest request, List<String> roles) {
    return isEditorOrAdmin(roles);
  }

  @Override
  public boolean authorizeDelete(HttpServletRequest request, List<String> roles) {
    return isEditorOrAdmin(roles);
  }

  private boolean isEditorOrAdmin(List<String> roles) {
    return hasAdminRole(roles) || hasEditorRole(roles);
  }
}
