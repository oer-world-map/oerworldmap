package de.oer.oerworldmap.controllers.auth;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class OIDCHeaderInterceptor implements HandlerInterceptor {
  public static final String REQUEST_USERNAME_ATTRIBUTE = "username";
  private static final String OIDC_CLAIM_SUB = "oidc_claim_sub";
  private static final String OIDC_CLAIM_EMAIL = "oidc_claim_email";

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    if (request.getHeader(OIDC_CLAIM_SUB) != null
      && request.getHeader(OIDC_CLAIM_EMAIL) != null
    ) {
      request.setAttribute(
        REQUEST_USERNAME_ATTRIBUTE,
        request.getHeader(OIDC_CLAIM_EMAIL));
    }
    return HandlerInterceptor.super.preHandle(request, response, handler);
  }
}
