package de.oer.oerworldmap.controllers.auth;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.oer.oerworldmap.controllers.OERWorldMap;
import de.oer.oerworldmap.models.Resource;
import de.oer.oerworldmap.services.AccountService;
import jakarta.servlet.http.HttpServletRequest;

/**
 * Authorizer interceptor for requests to UserIndex rest controller
 */
@Component
public class UserAuthorizerInterceptor implements AuthorizerInterceptor {
  private static Logger logger = LoggerFactory.getLogger(UserAuthorizerInterceptor.class);
  private AccountService accountService;

  @Autowired
  public UserAuthorizerInterceptor(AccountService accountService) throws IOException {
    this.accountService = accountService;
  }

  @Override
  public boolean authorizeGet(HttpServletRequest request, List<String> roles) {
    //Anyone is allowed to view profiles
    return true;
  }

  @Override
  public boolean authorizePost(HttpServletRequest request, List<String> roles) {
    Resource payload;
    try {
      payload = Resource.fromJson(OERWorldMap.getJsonFromRequest(request));
    } catch (IOException e) {
      throw new RuntimeException("Could not read request payload");
    }
    String username = request.getHeader(OIDC_CLAIM_EMAIL);
    String profileId = this.accountService.getProfileId(username);
    logger.info("Authorizing profile create/edit for {}", username);
    if (!doesUserExist(username, accountService)) {
      logger.info("User does not exist");
      return false;
    }
    //User may only create/update their own profile
    if (!payload.getId().equals(profileId)) {
      return false;
    }
    return true;
  }

  @Override
  public boolean authorizePut(HttpServletRequest request, List<String> roles) {
    //Currently not in use
    return false;
  }

  @Override
  public boolean authorizeDelete(HttpServletRequest request, List<String> roles) {
    String username = request.getHeader(OIDC_CLAIM_EMAIL);
    if (!doesUserExist(username, accountService)) {
      return false;
    }
    //Only admins may delete profiles
    return roles.contains(ROLE_ADMIN);
  }
}
