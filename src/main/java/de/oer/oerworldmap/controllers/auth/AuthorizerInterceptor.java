package de.oer.oerworldmap.controllers.auth;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.oer.oerworldmap.services.AccountService;
import de.oer.oerworldmap.services.KeycloakAccountService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public interface AuthorizerInterceptor extends HandlerInterceptor {

  static final String REQUEST_USERNAME_ATTRIBUTE = "username";

  static final String OIDC_CLAIM_EMAIL = "oidc_claim_email";
  static final String OIDC_CLAIM_REALM_ACCESS = "oidc_claim_realm_access";

  static final String METHOD_GET = "GET";
  static final String METHOD_POST = "POST";
  static final String METHOD_PUT = "PUT";
  static final String METHOD_DELETE = "DELETE";

  static final String ROLE_ADMIN = "admin";
  static final String ROLE_EDITOR = "editor";
  static final String ROLES = "roles";

  default boolean preHandle(HttpServletRequest request, HttpServletResponse response,
      Object handler) throws Exception {
    boolean authorized = authorize(request);
    if(!authorized) {
      response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
      return false;
    }
    return HandlerInterceptor.super.preHandle(request, response, handler);
  }

  default boolean authorize(HttpServletRequest request) throws JsonMappingException, JsonProcessingException {
    List<String> roles = getRolesFromHeader(request.getHeader(OIDC_CLAIM_REALM_ACCESS));
    return switch (request.getMethod().toUpperCase()) {
      case METHOD_DELETE -> authorizeDelete(request, roles);
      case METHOD_POST -> authorizePost(request, roles);
      case METHOD_GET -> authorizeGet(request, roles);
      case METHOD_PUT -> authorizePut(request, roles);
      default -> true;
    };
  }

  default List<String> getRolesFromHeader(String realmAccessHeader)
      throws JsonMappingException, JsonProcessingException {
    List<String> roles = new ArrayList<String>();
    if (realmAccessHeader == null || realmAccessHeader.isEmpty()) {
      return roles;
    }
    ObjectMapper mapper = new ObjectMapper();
    JsonNode obj = mapper.readTree(realmAccessHeader);
    if (obj.has(ROLES)) {
      obj.get(ROLES).forEach(node -> roles.add(node.asText()));
    }
    return roles;
  };

  default boolean hasAdminRole(List<String> roles) {
    return roles.contains(ROLE_ADMIN);
  }

  default boolean hasEditorRole(List<String> roles) {
    return roles.contains(ROLE_EDITOR);
  }

  default boolean doesProfileExist(String username, AccountService accountService) {
    return accountService.getProfileId(username) != null;
  }

  default boolean doesUserExist(String username, AccountService accountService) {
    boolean active = true;
    if (accountService instanceof KeycloakAccountService) {
      active = ((KeycloakAccountService) accountService).isUserActive(username);
    }
    return active;
  }

  boolean authorizeGet(HttpServletRequest request, List<String> roles);

  boolean authorizePost(HttpServletRequest request, List<String> roles);

  boolean authorizePut(HttpServletRequest request, List<String> roles);

  boolean authorizeDelete(HttpServletRequest request, List<String> roles);
}
