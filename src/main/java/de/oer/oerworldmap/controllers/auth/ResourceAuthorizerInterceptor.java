package de.oer.oerworldmap.controllers.auth;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;
import com.fasterxml.jackson.databind.node.ArrayNode;

import de.oer.oerworldmap.models.Resource;
import de.oer.oerworldmap.repository.BaseRepository;
import de.oer.oerworldmap.services.AccountService;
import jakarta.servlet.http.HttpServletRequest;

/**
 * Authorizer interceptor for requests to ResourceIndex rest controller
 */
@Component
public class ResourceAuthorizerInterceptor implements AuthorizerInterceptor {

  private static Logger logger = LoggerFactory.getLogger(ResourceAuthorizerInterceptor.class);

  private BaseRepository baseRepository;

  private AccountService accountService;

  @Autowired
  public ResourceAuthorizerInterceptor(AccountService accountService, BaseRepository baseRepository) throws IOException {
    this.accountService = accountService;
    this.baseRepository = baseRepository;
  }

  @Override
  public boolean authorizeDelete(HttpServletRequest request, List<String> roles) {
    String username = request.getHeader(OIDC_CLAIM_EMAIL);
    if (!doesUserExist(username, accountService)) {
      return false;
    }
    @SuppressWarnings("unchecked")
    final Map<String, String> pathVariables = (Map<String, String>) request
      .getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
    String id = pathVariables.get("id");
    Resource resource = baseRepository.getResource(id);
    //Admins may delete every resource
    if (roles.contains(ROLE_ADMIN)) {
      logger.debug("User is admin");
      return true;
    }

    //Users may delete their own likes
    if (resource.getType().equals("LikeAction")) {
      if (username == null || username.isEmpty()) {
        return false;
      }
      String profileId = this.accountService.getProfileId(username);
      AtomicBoolean ownLike = new AtomicBoolean(false);
      ArrayNode agents = (ArrayNode) resource.toJson().get("agent");
      agents.forEach(agent -> {
        if (agent.get("@id").asText().equals(profileId)) {
          ownLike.set(ownLike.get() || true);
        }
      });
      return ownLike.get();
    }
    return false;
  }

  @Override
  public boolean authorizeGet(HttpServletRequest request, List<String> roles) {
    return true;
  }

  @Override
  public boolean authorizePost(HttpServletRequest request, List<String> roles) {
    String username = request.getHeader(OIDC_CLAIM_EMAIL);
    if (!doesUserExist(username, accountService)) {
      logger.warn("User " + username + " does not exist");
      return false;
    }
    return true;
  }

  @Override
  public boolean authorizePut(HttpServletRequest request, List<String> roles) {
    String username = request.getHeader(OIDC_CLAIM_EMAIL);
    if (!doesUserExist(username, accountService)) {
      logger.warn("User " + username + " does not exist");
      return false;
    }
    return true;
}
}
