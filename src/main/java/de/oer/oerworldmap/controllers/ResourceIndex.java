package de.oer.oerworldmap.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ListProcessingReport;
import com.github.fge.jsonschema.core.report.ProcessingReport;

import de.oer.oerworldmap.config.Config;
import de.oer.oerworldmap.helpers.JsonLdConstants;
import de.oer.oerworldmap.helpers.MimeTypes;
import de.oer.oerworldmap.models.Commit;
import de.oer.oerworldmap.models.MetadataRecord;
import de.oer.oerworldmap.models.Resource;
import de.oer.oerworldmap.models.ResourceList;
import de.oer.oerworldmap.models.TripleCommit;
import de.oer.oerworldmap.repository.BaseRepository;
import de.oer.oerworldmap.repository.QueryContext;
import de.oer.oerworldmap.repository.TriplestoreRepository.InvalidResourceException;
import de.oer.oerworldmap.services.AccountService;
import de.oer.oerworldmap.services.SearchConfig;
import de.oer.oerworldmap.services.exporter.CalendarExporter;
import de.oer.oerworldmap.services.exporter.CsvExporter;
import de.oer.oerworldmap.services.exporter.GeoJsonExporter;
import de.oer.oerworldmap.services.exporter.JsonSchemaExporter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.server.NotAcceptableStatusException;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author fo
 */
@RestController
public class ResourceIndex extends OERWorldMap {

  private static Logger logger = LoggerFactory.getLogger(ResourceIndex.class);
  private static final String EXPORT_FILENAME = "export";

  private GeoJsonExporter mGeoJsonExporter = new GeoJsonExporter();

  @Autowired
  public ResourceIndex(Config aConf, BaseRepository baseRepository, AccountService accountService) {
    super(aConf, baseRepository, accountService);
  }

  @GetMapping("resource/")
  public ResponseEntity<String> list(
      @RequestParam(required = false) String q,
      @RequestParam(required = false, defaultValue = "0") Integer from,
      @RequestParam(required = false, defaultValue = "20") Integer size,
      @RequestParam(required = false) String sort,
      @RequestParam(required = false) String ext,
      @RequestParam(required = false) String iso3166,
      @RequestParam(required = false) String region,
      @RequestParam(required = false) String disposition,
      HttpServletRequest request, HttpServletResponse response) {

    Map<String, List<String>> filters = new HashMap<>();
    QueryContext queryContext = getQueryContext(request);
    HttpHeaders responseHeaders = new HttpHeaders();

    // Handle ISO 3166 param
    if (!StringUtils.isEmpty(iso3166)) {
      if (!Arrays.asList(Locale.getISOCountries()).contains(iso3166.toUpperCase())) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).build();
      }
      queryContext.setIso3166Scope(iso3166.toUpperCase());
    }

    // Handle region param
    if (!StringUtils.isEmpty(iso3166) && !StringUtils.isEmpty(region)) {
      queryContext.setRegionScope(iso3166.toUpperCase().concat(".").concat(region.toUpperCase()));
    }

    // Extract filters directly from query params
    Pattern filterPattern = Pattern.compile("^filter\\.(.*)$");
    for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
      Matcher filterMatcher = filterPattern.matcher(entry.getKey());
      if (filterMatcher.find()) {
        ArrayList<String> filter = new ArrayList<>();
        for (String value : entry.getValue()) {
          try {
            JsonNode jsonNode = mObjectMapper.readTree(value);
            if (jsonNode.isArray()) {
              for (JsonNode e : jsonNode) {
                filter.add(e.textValue());
              }
            } else {
              filter.add(jsonNode.textValue());
            }
          } catch (IOException e) {
            filter.add(value);
          }
        }
        filters.put(filterMatcher.group(1), filter);
      }
    }

    // Handle special filter case for event calendar
    if (filters.containsKey("about.@type")
      && filters.get("about.@type").size() == 1
      && filters.get("about.@type").contains("Event")
      && !filters.containsKey("about.startTime.GTE")
    ) {
      filters.put("about.startTime.GTE", Collections.singletonList("now/d"));
    } else if (filters.containsKey("about.@type")
      && (!filters.get("about.@type").contains("Event")
        || filters.get("about.@type").size() != 1)
    ) {
      filters.remove("about.startTime.GTE");
    }

    // Check for bounding box
    String boundingBox = request.getParameter("boundingBox");
    if (boundingBox != null) {
      if (boundingBox != null) {
        try {
          queryContext.setBoundingBox(boundingBox);
        } catch (NumberFormatException e) {
          logger.trace("Invalid bounding box: ".concat(boundingBox), e);
        }
      }
    }

    // Check for fields to fetch
    if (request.getParameterMap().containsKey("fields")) {
      queryContext.setFetchSource(request.getParameterValues("fields"));
    }
    String searchConfigFile = mConf.search().confFile();
    SearchConfig searchConfig = searchConfigFile != null
      ? new SearchConfig(searchConfigFile)
      : new SearchConfig();
    queryContext.setElasticsearchFieldBoosts(searchConfig.getBoostsForElasticsearch());
    ResourceList resourceList = mBaseRepository.query(q, from, size, sort, filters, queryContext);

    String baseUrl = mConf.proxy().host();
    String filterString = "";
    for (Map.Entry<String, List<String>> filter : filters.entrySet()) {
      String filterKey = "filter.".concat(filter.getKey());
      for (String filterValue : filter.getValue()) {
        //If filter value is invalid: return error 400
        if (filterValue == null) {
          return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        try {
          filterString = filterString.concat("&".concat(filterKey).concat("=").concat(URLEncoder.encode(filterValue,
            StandardCharsets.UTF_8.name())));
        } catch (UnsupportedEncodingException e) {
          logger.error("Unhandled encoding", e);
        }
      }
    }

    Set<String> alternates = MimeTypes.all().keySet();
    if (!resourceList.containsType("Event")) {
      alternates.remove("ics");
    }
    List<String> links = new ArrayList<>();
    for (String alternate : alternates) {
      String url = getUri( "list",
        q, 0, -1, sort, alternate, iso3166, region, disposition, request, response);
      String linkUrl = baseUrl.concat(url.concat(filterString));
      links.add(String.format("<%s>; rel=\"alternate\"; type=\"%s\"", linkUrl,
        MimeTypes.fromExtension(alternate)));
    }
    responseHeaders.add("Link", String.join(", ", links));

    String format = StringUtils.isEmpty(ext)
      ? MimeTypes.fromRequest(request)
      : MimeTypes.fromExtension(ext);
    String responseContent = null;
    String contentDisposition = "inline".equals(disposition) ? "inline" : "attachment";
    if (format == null) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
    switch (format) {
      case "text/csv":
        CsvExporter csvExporter = request.getHeader("X-CSV-HEADERS") != null
        ? new CsvExporter(Arrays.stream(request.getHeader("X-CSV-HEADERS").split(","))
          .map(Pattern::compile).collect(Collectors.toList()))
        : new CsvExporter();
        responseHeaders.setContentType(MediaType.parseMediaType("text/csv"));
        contentDisposition += ";filename=" + EXPORT_FILENAME + ".csv";
        responseContent = csvExporter.export(resourceList);
        break;
      case "text/calendar":
        responseHeaders.setContentType(MediaType.parseMediaType("text/calendar"));
        contentDisposition += ";filename=" + EXPORT_FILENAME + ".ics";
        responseContent = new CalendarExporter(Locale.ENGLISH).export(resourceList);
        break;
      case "application/json":
        Resource result = resourceList.toResource();
        if (!StringUtils.isEmpty(iso3166)) {
          if (!StringUtils.isEmpty(iso3166)) {
            result.put("iso3166", iso3166.toUpperCase());
          }
        }
        JsonNode rString = result.toJson();
        contentDisposition += ";filename=" + EXPORT_FILENAME + ".json";
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
        responseContent = rString.toString();
        break;
      case "application/geo+json":
        responseHeaders.setContentType(MediaType.parseMediaType("application/geo+json"));
        contentDisposition += ";filename=" + EXPORT_FILENAME + ".geojson";
        responseContent = mGeoJsonExporter.export(resourceList);
        break;
      case "application/schema+json":
        responseHeaders.setContentType(MediaType.parseMediaType("application/schema+json"));
        responseContent = new JsonSchemaExporter().export(resourceList);
        break;
    }
    if (!StringUtils.isEmpty(ext)) {
      responseHeaders.add("Content-Disposition", contentDisposition);
    }
    if (responseContent != null) {
      return new ResponseEntity<String>(responseContent, responseHeaders, HttpStatus.OK);
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
  }

  @PostMapping
  @RequestMapping("import/")
  public ResponseEntity<String> importResources(HttpServletRequest request) throws IOException {
    JsonNode json = getJsonFromRequest(request);
    List<Resource> resources = new ArrayList<>();
    if (json.isArray()) {
      for (JsonNode node : json) {
        resources.add(Resource.fromJson(node));
      }
    } else if (json.isObject()) {
      resources.add(Resource.fromJson(json));
    } else {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
    mBaseRepository.importResources(resources, getMetadata(request));
    CachedInterceptor.updateEtag();
    return ResponseEntity.ok(Integer.toString(resources.size()).concat(" resources imported."));
  }

  @PostMapping("resource/")
  public ResponseEntity addResource(
      HttpServletRequest request, HttpServletResponse response) throws IOException {
    JsonNode jsonNode = getJsonFromRequest(request);
    if (jsonNode == null || (!jsonNode.isArray() && !jsonNode.isObject())) {
      return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body("Bad or empty JSON");
    } else if (jsonNode.isArray()) {
      return upsertResources(jsonNode, request);
    } else {
      return upsertResource(jsonNode, request, response, false);
    }
  }

  @PostMapping("resource/{id}")
  public ResponseEntity updateResource(@PathVariable String id,
      HttpServletRequest request, HttpServletResponse response) throws IOException {
    // If updating a resource, check if it actually exists
    Resource originalResource = mBaseRepository.getResource(id);
    if (originalResource == null) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found: ".concat(id));
    }
    return upsertResource(request, response, true);
  }

  private ResponseEntity upsertResource(
      HttpServletRequest request, HttpServletResponse response, boolean isUpdate) throws IOException {
    JsonNode jsonNode = getJsonFromRequest(request);
    return upsertResource(jsonNode, request, response, isUpdate);
  }

  private ResponseEntity upsertResource(
      JsonNode jsonNode, HttpServletRequest request, HttpServletResponse response, boolean isUpdate) throws IOException {
    if (jsonNode == null || (!jsonNode.isArray() && !jsonNode.isObject())) {
      return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body("Bad or empty JSON");
    }
    // Extract resource
    Resource resource = Resource.fromJson(jsonNode);
    resource.put(JsonLdConstants.CONTEXT, mConf.jsonld().context());

    // Person create /update only through UserIndex, which is restricted to admin
    if (!isUpdate && "Person".equals(resource.getType())) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Upsert Person forbidden.");
    }

    // Validate
    Resource staged;
    try {
      staged = mBaseRepository.stage(resource);
    } catch (InvalidResourceException ire) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ire.getMessage());
    }
    ProcessingReport processingReport = validate(staged);
    if (!processingReport.isSuccess()) {
      ListProcessingReport listProcessingReport = new ListProcessingReport();
      try {
        listProcessingReport.mergeWith(processingReport);
      } catch (ProcessingException e) {
        logger.warn("Failed to create list processing report", e);
      }
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(listProcessingReport.asJson());
    }

    // Save
    mBaseRepository.addResource(resource, getMetadata(request));
    CachedInterceptor.updateEtag();

    // Respond
    if (isUpdate) {
      return ResponseEntity.ok(getRecord(mBaseRepository.getResource(resource.getId())).toJson());
    } else {
      String url = getUri("read", resource.getId(), null, null, null ,null, null);
      response.setHeader(HttpHeaders.LOCATION, url);
      return ResponseEntity.status(HttpStatus.CREATED)
        .body(getRecord(mBaseRepository.getResource(resource.getId())).toJson());
    }
  }

  private ResponseEntity upsertResources(HttpServletRequest request) throws IOException {
    JsonNode jsonNodes = getJsonFromRequest(request);
    return upsertResources(jsonNodes, request);
  }

  private ResponseEntity upsertResources(JsonNode jsonNodes, HttpServletRequest request) throws IOException {
    // Extract resources
    List<Resource> resources = new ArrayList<>();
    for (JsonNode jsonNode : jsonNodes) {
      Resource resource = Resource.fromJson(jsonNode);
      resource.put(JsonLdConstants.CONTEXT, mConf.jsonld().context());
      resources.add(resource);
    }

    // Validate
    ListProcessingReport listProcessingReport = new ListProcessingReport();
    for (Resource resource : resources) {
      // Person create /update only through UserIndex, which is restricted to admin
      if ("Person".equals(resource.getType())) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Upsert Person forbidden.");
      }
      // Stage and validate each resource
      try {
        Resource staged = mBaseRepository.stage(resource);
        ProcessingReport processingMessages = validate(staged);
        if (!processingMessages.isSuccess()) {
          logger.debug(processingMessages.toString());
          logger.debug(staged.toString());
        }
        listProcessingReport.mergeWith(processingMessages);
      } catch (ProcessingException e) {
        logger.error("Could not process validation report", e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
      }
    }

    if (!listProcessingReport.isSuccess()) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(listProcessingReport.asJson());
    }
    mBaseRepository.addResources(resources, getMetadata(request));
    CachedInterceptor.updateEtag();

    return ResponseEntity.ok("Added resources");
  }

  @GetMapping("resource/{id}")
  public ResponseEntity read(@PathVariable String id,
      @RequestParam(required = false, defaultValue = "HEAD") String version,
      @RequestParam (required = false) String extension,
      @RequestParam(required = false, defaultValue = "attachment") String disposition,
      HttpServletRequest request, HttpServletResponse response) {

    // Ignore file extension
    if (id.contains(".")) {
      id = id.substring(0, id.indexOf("."));
    }
    Resource resource = mBaseRepository.getResource(id, version);
    if (null == resource) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found");
    }
    HttpHeaders responseHeaders = new HttpHeaders();
    Set<String> alternates = MimeTypes.all().keySet();
    if (!"Event".equals(resource.getType())) {
      alternates.remove("ics");
    }
    List<String> links = new ArrayList<>();
    String baseUrl = mConf.proxy().host() != null ? mConf.proxy().host(): "";
    for (String alternate : alternates) {
      String linkUrl = getUri("read",
        id, version, alternate, disposition, null, null);
      links.add(String.format("<%s>; rel=\"alternate\"; type=\"%s\"", baseUrl.concat(linkUrl),
        MimeTypes.fromExtension(alternate)));
    }

    responseHeaders.add("Link", String.join(", ", links));

    String format = StringUtils.isEmpty(extension)
      ? MimeTypes.fromRequest(request)
      : MimeTypes.fromExtension(extension);

    resource = getRecord(resource);
    String responseContent = null;
    String contentDisposition = "inline".equals(disposition) ? "inline" : "attachment";
    if (format == null) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
    switch (format) {
      case "text/csv":
        CsvExporter csvExporter = request.getHeader("X-CSV-HEADERS") != null
        ? new CsvExporter(Arrays.stream(request.getHeader("X-CSV-HEADERS").split(","))
          .map(Pattern::compile).collect(Collectors.toList()))
        : new CsvExporter();
        responseHeaders.setContentType(MediaType.parseMediaType("text/csv"));
        contentDisposition += ";filename=" + EXPORT_FILENAME + ".csv";
        responseContent = csvExporter.export(resource);
        break;
      case "text/calendar":
        responseHeaders.setContentType(MediaType.parseMediaType("text/calendar"));
        contentDisposition += ";filename=" + EXPORT_FILENAME + ".ics";
        responseContent = new CalendarExporter(Locale.ENGLISH).export(resource);
        break;
      case "application/json":
        contentDisposition += ";filename=" + EXPORT_FILENAME + ".json";
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
        responseContent = resource.toString();
        break;
      case "application/geo+json":
        responseHeaders.setContentType(MediaType.parseMediaType("application/geo+json"));
        contentDisposition += ";filename=" + EXPORT_FILENAME + ".geojson";
        String geoJson = mGeoJsonExporter.export(resource);
        if (geoJson != null) {
          responseContent = mGeoJsonExporter.export(resource);
        } else {
          throw new NotAcceptableStatusException(resource.toString());
        }
        break;
      case "application/schema+json":
        responseHeaders.setContentType(MediaType.parseMediaType("application/schema+json"));
        responseContent = new JsonSchemaExporter().export(resource);
        break;
    }
    if (!StringUtils.isEmpty(extension)) {
      responseHeaders.add("Content-Disposition", contentDisposition);
    }
    if (responseContent != null) {
      return new ResponseEntity<String>(responseContent, responseHeaders, HttpStatus.OK);
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
  }

  private MetadataRecord getRecord(Resource aResource) {
    MetadataRecord metadataRecord = new MetadataRecord(aResource);
    List<Commit> history = mBaseRepository.log(aResource.getId());
    metadataRecord.put(MetadataRecord.CONTRIBUTOR, mAccountService.getProfileId(history.get(0).getHeader().getAuthor()));
    try {
      metadataRecord.put(MetadataRecord.AUTHOR, mAccountService.getProfileId(history.get(history.size() - 1).getHeader().getAuthor()));
    } catch (NullPointerException e) {
      logger.trace("Could not read author from commit " + history.get(history.size() - 1), e);
    }
    metadataRecord.put(MetadataRecord.DATE_MODIFIED, history.get(0).getHeader().getTimestamp()
      .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
    try {
      metadataRecord.put(MetadataRecord.DATE_CREATED, history.get(history.size() - 1).getHeader().getTimestamp()
        .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
    } catch (NullPointerException e) {
      logger.trace("Could not read timestamp from commit " + history.get(history.size() - 1), e);
    }
    return metadataRecord;
  }

  @DeleteMapping("resource/{id}")
  public ResponseEntity delete(@PathVariable String id, HttpServletRequest request) throws IOException {
    return deleteResource(id, request);
  }

  @GetMapping({"log/", "log/{id}"})
  public ResponseEntity log(
      @PathVariable(required = false) String id,
      @RequestParam(defaultValue = "") String compare,
      @RequestParam(defaultValue = "") String to) {
    ArrayNode log = JsonNodeFactory.instance.arrayNode();
    List<Commit> commits = mBaseRepository.log(id);
    for (Commit commit : commits) {
      ObjectNode entry = JsonNodeFactory.instance.objectNode();
      entry.put("commit", commit.getId());
      entry.put("author", commit.getHeader().getAuthor());
      entry.put("date", commit.getHeader().getTimestamp().toString());
      entry.put("primary_topic", commit.getHeader().getPrimaryTopic());
      log.add(entry);
    }

    if (StringUtils.isEmpty(id)) {
      return ResponseEntity.ok(log);
    }
    String v1 = StringUtils.isEmpty(compare) ? log.get(0).get("commit").textValue() : compare;
    String v2 =
      StringUtils.isEmpty(to) ? log.get(log.size() > 1 ? 1 : 0).get("commit").textValue() : to;
    Resource r1 = getRecord(mBaseRepository.getResource(id, v1));
    Resource r2 = getRecord(mBaseRepository.getResource(id, v2));
    r1.put("version", v1);
    r2.put("version", v2);

    ObjectNode result = JsonNodeFactory.instance.objectNode();
    result.set("compare", r1.toJson());
    result.set("to", r2.toJson());
    result.set("log", log);
    return ResponseEntity.ok(result);
  }

  @PostMapping("index/{id}")
  public ResponseEntity<String> index(@PathVariable String id) {
    mBaseRepository.index(id);
    return ResponseEntity.ok("Indexed ".concat(id));
  }

  @PostMapping("resource/{id}/comment")
  public ResponseEntity<JsonNode> commentResource(@PathVariable String id,
      HttpServletRequest request) throws IOException {

    Resource resource = mBaseRepository.getResource(id);

    if (resource == null) {
      return ResponseEntity.notFound().build();
    }

    Resource user = getUser(request);
    // If the user hasn't created a profile page yet
    if (user == null) {
      return ResponseEntity.badRequest().build();
    }

    Resource comment = Resource.fromJson(getJsonFromRequest(request));
    //Check if text is not blank
    JsonNode text = comment.toJson().get("text");
    if (text == null) {
      return ResponseEntity.badRequest().build();
    }
    AtomicBoolean containsEmptyText = new AtomicBoolean(false);
    text.fieldNames().forEachRemaining(key -> {
      String value = text.get(key).asText();
      if (value == null || value.isEmpty()) {
        containsEmptyText.set(true);
      }
    });
    if (containsEmptyText.get()) {
      return ResponseEntity.badRequest().build();
    }
    //Add additional info
    comment.put(JsonLdConstants.CONTEXT, mConf.jsonld().context());
    comment.put("author", user);
    comment.put("dateCreated", ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
    comment.put("commentOn", resource);


    TripleCommit.Diff diff = (TripleCommit.Diff) mBaseRepository.getDiff(comment);

    Map<String, String> metadata = getMetadata(request);
    TripleCommit.Header header = new TripleCommit.Header(
      metadata.get(TripleCommit.Header.AUTHOR_HEADER),
      ZonedDateTime.parse(metadata.get(TripleCommit.Header.DATE_HEADER)));
    TripleCommit commit = new TripleCommit(header, diff);
    mBaseRepository.commit(commit);

    CachedInterceptor.updateEtag();
    return ResponseEntity.status(HttpStatus.CREATED).body(comment.toJson());
  }

  @GetMapping(value = {"label", "label/", "label/{id}"})
  public ResponseEntity<String> label(@PathVariable(required = false) String id)
      throws UnsupportedEncodingException {
    return ResponseEntity.ok(StringUtils.isEmpty(id)
      ? mBaseRepository.sparql(
      "SELECT DISTINCT * WHERE {?uri <http://schema.org/name> ?label}")
      : mBaseRepository.label(URLDecoder.decode(id, "UTF-8"))
    );
  }

  @GetMapping("activity/")
  public ResponseEntity<ArrayNode> activity(@RequestParam(required = false) String until) {
    List<Commit> activities = mBaseRepository.log(null);
    ArrayNode result = JsonNodeFactory.instance.arrayNode();
    String previousId = null;
    for (Commit commit : activities) {
      if (result.size() == 20 || (until != null && commit.getId().equals(until))) {
        break;
      }
      String id = ((TripleCommit) commit).getPrimaryTopic().getURI();
      TripleCommit.Header header = ((TripleCommit) commit).getHeader();
      // Skip empty commits, commits on same subject and deleted resources
      if (id == null
        || id.equals(previousId)
        || header.isMigration()
        || header.getAuthor().equals("System")
        || !mBaseRepository.hasResource(id))
      {
        continue;
      }
      previousId = id;
      Resource resource = mBaseRepository.getResource(id);
      ObjectNode entry = JsonNodeFactory.instance.objectNode();
      String profileId = mAccountService.getProfileId(commit.getHeader().getAuthor());
      if (profileId != null) {
        Resource user = mBaseRepository.getResource(profileId);
        if (user != null) {
          entry.set("user", user.toJson());
        }
      }
      ObjectNode action = JsonNodeFactory.instance.objectNode();
      action.put("time", commit.getHeader().getTimestamp().toString());
      action.put("type", (mBaseRepository.log(id).size() > 1 ? "edit" : "add"));
      entry.set("about", resource.toJson());
      entry.set("action", action);
      entry.put("id", commit.getId());
      result.add(entry);
    }
    return ResponseEntity.ok(result);
  }

  private String getUri(String methodName, Object... args) {
    UriComponentsBuilder builder = MvcUriComponentsBuilder.fromController(getClass());
    MvcUriComponentsBuilder mvcBuilder = MvcUriComponentsBuilder.relativeTo(builder);
    return mvcBuilder.withMethodName(ResourceIndex.class, methodName, args)
      //Clear scheme, host and port
      .host(null).port(null).scheme(null)
      .toUriString();
  }
}
