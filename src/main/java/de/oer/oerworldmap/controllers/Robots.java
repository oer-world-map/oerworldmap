package de.oer.oerworldmap.controllers;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("robots.txt")
public class Robots {

  @Value("${web.crawling.allowed}")
  private boolean crawlingAllowed;

  @GetMapping()
  public String get() {
    StringWriter stringWriter = new StringWriter();
    PrintWriter writer = new PrintWriter(stringWriter, true);
    writer.println("User-agent: *");
    if (crawlingAllowed) {
      writer.println("Disallow:");
    } else {
      writer.println("Disallow: /");
    }
    return stringWriter.toString();
  }
}
