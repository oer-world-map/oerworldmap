package de.oer.oerworldmap.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.oer.oerworldmap.config.Config;
import de.oer.oerworldmap.models.Resource;
import de.oer.oerworldmap.repository.BaseRepository;
import de.oer.oerworldmap.repository.QueryContext;
import de.oer.oerworldmap.services.AccountService;
import de.oer.oerworldmap.services.SearchConfig;
import jakarta.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author pvb
 */
@RestController
@RequestMapping("reconcile")
public class Reconciler extends OERWorldMap {
  private static final Logger logger = LoggerFactory.getLogger(Reconciler.class);

  @Autowired
  public Reconciler(Config aConf, BaseRepository repository, AccountService accountService) {
    super(aConf, repository, accountService);
  }

  @GetMapping
  public ResponseEntity<Object> meta(String aCallback) {
    ObjectNode result = mObjectMapper.createObjectNode();
    result.put("name", "oerworldmap reconciliation");
    result.put("identifierSpace", "https://oerworldmap.org/resource");
    result.put("schemaSpace", "http://schema.org");
    ArrayNode defaultTypes = mObjectMapper.createArrayNode();
    Resource.mIdentifiedTypes.forEach(x -> {
      ObjectNode defaultType = mObjectMapper.createObjectNode();
      defaultType.put("id", x);
      defaultType.put("name", x);
      defaultTypes.add(defaultType);
    });
    result.set("defaultTypes", defaultTypes);
    result.set("view", mObjectMapper.createObjectNode().put("url", "https://oerworldmap.org/resource/{{id}}"));
    Object responseData = StringUtils.isEmpty(aCallback) ? result
    : String.format("/**/%s(%s);", aCallback, result.toString());
    return ResponseEntity.ok(responseData);
  }

  @PostMapping
  public JsonNode reconcile(HttpServletRequest httpServletRequestrequest)
      throws JsonProcessingException {
    JsonNode request = mObjectMapper.readTree(
        httpServletRequestrequest.getParameter("queries"));
    Iterator<Map.Entry<String, JsonNode>> inputQueries = request.fields();
    return reconcile(inputQueries, null, Locale.ENGLISH, httpServletRequestrequest); // TODO: fetch Locale from UI
  }


  public JsonNode reconcile(final Iterator<Map.Entry<String, JsonNode>> aInputQueries,
    final QueryContext aQueryContext, final Locale aPreferredLocale,
    HttpServletRequest request) {
    QueryContext queryContext = aQueryContext != null ? aQueryContext : getQueryContext(request);
    String searchConfigFile = mConf.reconcile().confFile();
    SearchConfig searchConfig = searchConfigFile != null
      ? new SearchConfig(searchConfigFile)
      : new SearchConfig();
    queryContext.setElasticsearchFieldBoosts(searchConfig.getBoostsForElasticsearch());
    ObjectNode response = mObjectMapper.createObjectNode();

    try {
      while (aInputQueries.hasNext()) {
        Map.Entry<String, JsonNode> inputQuery = aInputQueries.next();
        JsonNode limitNode = inputQuery.getValue().get("limit");
        int limit = limitNode == null ? -1 : limitNode.asInt();
        String queryString = inputQuery.getValue().get("query").asText();
        JsonNode type = inputQuery.getValue().get("type");
        Map<String, List<String>> typeFilter = new HashMap<>();
        if (type != null) {
          typeFilter.put("about.@type", Arrays.asList(type.asText()));
        }
        JsonNode reconciled = mBaseRepository
          .reconcile(queryString, 0, limit, null, typeFilter, queryContext, aPreferredLocale);
        response.set(inputQuery.getKey(), reconciled);
      }
    } catch (IOException ioe) {
      logger.error("Could not query base repository.", ioe);
    }
    return response;
  }
}
