package de.oer.oerworldmap.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.oer.oerworldmap.config.Config;
import de.oer.oerworldmap.controllers.auth.AuthorizerInterceptor;
import de.oer.oerworldmap.controllers.auth.OIDCHeaderInterceptor;
import de.oer.oerworldmap.helpers.JsonLdConstants;
import de.oer.oerworldmap.models.MetadataRecord;
import de.oer.oerworldmap.models.Resource;
import de.oer.oerworldmap.repository.BaseRepository;
import de.oer.oerworldmap.services.AccountService;
import de.oer.oerworldmap.services.KeycloakAccountService;
import de.oer.oerworldmap.services.MemoryAccountService;
import jakarta.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("user")
public class UserIndex extends OERWorldMap {

  private static Logger logger = LoggerFactory.getLogger(UserIndex.class);

  private static final String OIDC_CLAIM_PROFILE_ID = "oidc_claim_profile_id";
  private static final String OIDC_CLAIM_GROUPS = "oidc_claim_groups";
  private static final String OIDC_CLAIM_NAME = "oidc_claim_name";
  private static final String OIDC_CLAIM_SUB = "oidc_claim_sub";

  private String onboardingTokenName;
  private String deleteReasonLogPath;
  private DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ISO_INSTANT;


  @Autowired
  public UserIndex(Config aConf, BaseRepository baseRepository, AccountService accountService) {
    super(aConf, baseRepository, accountService);
    if (aConf.user().deleteReasonLog() != null) {
      deleteReasonLogPath = aConf.user().deleteReasonLog();
    } else {
      deleteReasonLogPath =
        System.getProperty("user.home")
        + FileSystems.getDefault().getSeparator()
        + "oerworldmap_delete_reason_log";
    }
    onboardingTokenName = aConf.user().onboardingTokenName();
  }

  @GetMapping("/profile")
  public ResponseEntity<JsonNode> getProfile(
    @RequestAttribute(name = OIDCHeaderInterceptor.REQUEST_USERNAME_ATTRIBUTE)
      String username,
    HttpServletRequest request) {
    String profileId = request.getHeader(OIDC_CLAIM_PROFILE_ID);
    //If profile id was not sent: Check in keycloak in case the profile is newly created and not yet included as header
    if (profileId == null) {
      if (this.mAccountService instanceof MemoryAccountService) {
        logger.error("No connection to keycloak");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
      }
      KeycloakAccountService accService = (KeycloakAccountService) this.mAccountService;
      profileId = accService.getProfileId(username);
    }
    Resource profile;
    boolean persistent = false;
    if (StringUtils.isEmpty(profileId)) {
      logger.debug("profileId (OIDC Claim) is empty, created new profile from request");
      profile = newProfile(request);
    } else {
      logger.debug("profileId (OIDC Claim) exists, fetching profile from database using profileId: " + profileId);
      profile = mBaseRepository.getResource(profileId);
      if (profile == null) {
        logger.warn("No profile found in database for " + profileId);
        profile = newProfile(request);
      } else {
        persistent = true;
      }
    }
    return ResponseEntity.ok(wrapProfile(profile, persistent, request));
  }

  @PostMapping("/profile")
  public ResponseEntity<JsonNode> createProfile(
      @RequestAttribute(name = OIDCHeaderInterceptor.REQUEST_USERNAME_ATTRIBUTE)
      String username,
      HttpServletRequest request) throws IOException {
    Resource profile = Resource.fromJson(getJsonFromRequest(request));
    boolean isNew = !mBaseRepository.hasResource(profile.getId());
    mBaseRepository.addResource(profile, getMetadata(request));
    if (isNew) {
      mAccountService.setProfileId(username, profile.getId());
    }
    ObjectNode profileJson = wrapProfile(profile, true, request);
    HttpStatus status = isNew ? HttpStatus.CREATED : HttpStatus.OK;
    return ResponseEntity
        .status(status)
        .body(profileJson);
  }

  /**
   * Delete a user using a one time token for authorization.
   *
   * Example request url:
   * user/token?username=example@some.domain&token=abcd-1234&reason=someReason
   *
   * If the optional reason is set, the string will be written into a log file
   * @param token Auth-Token
   * @param username Username
   * @param reason Optional reason for deletion
   * @param request Request
   * @return 200 if deleted successfully, 403 if the token is not recognized
   * @throws IOException
   */
  @DeleteMapping("/token")
  public ResponseEntity<String> deleteWithToken(
    @RequestParam(required = true) String token,
    @RequestParam(required = true) String username,
    @RequestParam(required = false, defaultValue = "") String reason,
    HttpServletRequest request
  ) throws IOException {
    //Check token
    if (this.mAccountService instanceof MemoryAccountService) {
      logger.error("No connection to keycloak");
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
    KeycloakAccountService accService = (KeycloakAccountService) this.mAccountService;
    String keycloakToken = accService.getUserAttribute(username, onboardingTokenName);
    if (keycloakToken == null || !keycloakToken.equals(token)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    //Delete user
    //If user has a profile: Delete it
    String profileId = accService.getProfileId(username);
    if (profileId != null) {
      request.setAttribute(AuthorizerInterceptor.REQUEST_USERNAME_ATTRIBUTE, username);
      HttpStatusCode deleteResult = deleteResource(profileId, request).getStatusCode();
      if (!deleteResult.equals(HttpStatus.OK)) {
        return ResponseEntity.status(deleteResult).build();
      }
    } else {
      this.mAccountService.deleteUser(username);
    }

    //Store reason if set
    if (!reason.isBlank()) {
      try (PrintWriter writer = new PrintWriter(
        new FileOutputStream(
          new File(deleteReasonLogPath),
          true))
      ) {
        String now = ZonedDateTime.now(ZoneOffset.UTC).format(DATE_FORMAT);
        String entry = String.format("%s - %s\n", now, reason);
        writer.append(entry);
      } catch (FileNotFoundException e) {
        logger.error("Could not open reason log file");
      }
    }

    return ResponseEntity.ok().build();
  }

  /**
   * Send an user an email for a password reset, using a one time token for authorization.
   *
   * Example request url:
   * user/token?username=example@some.domain&token=abcd-1234&reason=someReason
   *
   * If the optional reason is set, the string will be written into a log file
   * @param token Auth-Token
   * @param username Username
   * @param request Request
   * @return 200 if deleted successfully, 403 if the token is not recognized
   * @throws IOException
   */
  @PostMapping("/token")
  public ResponseEntity<String> resetWithToken(
    @RequestParam(required = true) String token,
    @RequestParam(required = true) String username,
    HttpServletRequest request
  ) throws IOException {
    //Check token
    if (this.mAccountService instanceof MemoryAccountService) {
      logger.error("No connection to keycloak");
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
    KeycloakAccountService accService = (KeycloakAccountService) this.mAccountService;
    String keycloakToken = accService.getUserAttribute(username, onboardingTokenName);
    if (keycloakToken == null || !keycloakToken.equals(token)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    // Send Password Reset E-Mail
    accService.sendPasswordReset(username);

    return ResponseEntity.ok().build();
  }

  private ObjectNode wrapProfile(Resource profile, boolean persistent, HttpServletRequest request) {
    String username = getUsernameFromRequest(request);
    ObjectNode result = mObjectMapper.createObjectNode();
    result.put("persistent", persistent);
    result.put("username", username);
    String groups = request.getHeader(OIDC_CLAIM_GROUPS);
    if (StringUtils.isEmpty(groups)) {
      result.set("groups", mObjectMapper.createArrayNode());
    } else {
      result.set("groups", mObjectMapper.valueToTree(groups.split(",")));
    }
    result.set("profile", new MetadataRecord(profile).toJson());
    result.put("id", profile.getId());
    return result;
  }

  private Resource newProfile(HttpServletRequest request) {
    ObjectNode profileNode = mObjectMapper.createObjectNode()
      .put(JsonLdConstants.CONTEXT, mConf.jsonld().context())
      .put("@type", "Person")
      .put("@id", "urn:uuid:".concat(request.getHeader(OIDC_CLAIM_SUB)))
      .put("email", getUsernameFromRequest(request));
    profileNode.set("name", mObjectMapper.createObjectNode()
      .put("en", request.getHeader(OIDC_CLAIM_NAME))
    );
    return(Resource.fromJson(profileNode));
  }
}
