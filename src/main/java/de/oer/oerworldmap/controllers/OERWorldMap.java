package de.oer.oerworldmap.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonschema.core.report.ProcessingReport;

import de.oer.oerworldmap.config.Config;
import de.oer.oerworldmap.controllers.auth.OIDCHeaderInterceptor;
import de.oer.oerworldmap.helpers.JsonSchemaValidator;
import de.oer.oerworldmap.models.Resource;
import de.oer.oerworldmap.models.ResourceList;
import de.oer.oerworldmap.models.TripleCommit;
import de.oer.oerworldmap.repository.BaseRepository;
import de.oer.oerworldmap.repository.QueryContext;
import de.oer.oerworldmap.services.AccountService;
import de.oer.oerworldmap.services.SearchConfig;
import jakarta.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author fo
 */
public abstract class OERWorldMap {
  private static Logger logger = LoggerFactory.getLogger(OERWorldMap.class);

  Config mConf;
  protected BaseRepository mBaseRepository;
  protected AccountService mAccountService;
  static final ObjectMapper mObjectMapper = new ObjectMapper();
  private static JsonSchemaValidator mSchemaValidator;

  private static synchronized void createSchemaValidator(Config aConf) {
    try {
      mSchemaValidator = new JsonSchemaValidator(
        Paths.get(aConf.json().schema()).toFile());
    } catch (IOException e) {
      logger.error("Could not read schema", e);
    }
  }

  protected OERWorldMap(Config aConf, BaseRepository baseRepository, AccountService accountService) {
    this.mConf = aConf;
    this.mBaseRepository = baseRepository;
    this.mAccountService = accountService;
    // JSON schema validator
    createSchemaValidator(mConf);
  }

  Resource getUser(HttpServletRequest request) {
    Resource user = null;
    String username = getUsernameFromRequest(request);
    logger.trace("Username %s", username);
    String profileId = mAccountService.getProfileId(username);
    if (!StringUtils.isEmpty(profileId)) {
      user = getRepository().getResource(profileId);
    }
    return user;
  }

  QueryContext getQueryContext(HttpServletRequest request) {
    List<String> roles = new ArrayList<>();
    roles.add("guest");
    if (getUser(request) != null) {
      roles.add("authenticated");
    }
    return new QueryContext(roles);
  }

  ProcessingReport validate(Resource aResource) {
    return mSchemaValidator.validate(aResource);
  }

  protected BaseRepository getRepository() {
    return mBaseRepository;
  }

  /**
   * Get resource from JSON body or form data
   *
   * @return The JSON node
   * @throws IOException 
   */
  public static JsonNode getJsonFromRequest(HttpServletRequest request) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    String jsonContent = IOUtils.toString(request.getReader());
    return mapper.readTree(jsonContent);
  }

  /**
   * Get metadata suitable for record provinence
   *
   * @return Map containing current getUser() in author and current time in date field.
   */
  protected Map<String, String> getMetadata(HttpServletRequest request) {
    Map<String, String> metadata = new HashMap<>();
    String username = getUsernameFromRequest(request);
    if (!StringUtils.isEmpty(username)) {
      metadata.put(TripleCommit.Header.AUTHOR_HEADER, username);
    } else {
      metadata.put(TripleCommit.Header.AUTHOR_HEADER, "System");
    }
    metadata.put(TripleCommit.Header.DATE_HEADER,
      ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
    return metadata;
  }

  public BaseRepository getBaseRepository() {
    return mBaseRepository;
  }

  protected String getUsernameFromRequest(HttpServletRequest request) {
    return (String) request.getAttribute(OIDCHeaderInterceptor.REQUEST_USERNAME_ATTRIBUTE);
  }

  protected ResponseEntity deleteResource(String id, HttpServletRequest request)
      throws IOException {
    Resource originalResource = mBaseRepository.getResource(id);
    if (originalResource != null && "Person".equals(originalResource.getType())) {
      return deleteProfile(originalResource, request);
    }
    Resource resource = mBaseRepository.deleteResource(id, getMetadata(request));
    ObjectNode result = JsonNodeFactory.instance.objectNode();
    CachedInterceptor.updateEtag();
    if (null != resource) {
        result.put("message", "deleted resource " + id);
        return ResponseEntity.ok(result);
    } else {
      result.put("message", "Failed to delete resource " + id);
      return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result);
    }
  }

  private ResponseEntity deleteProfile(Resource resource, HttpServletRequest request) throws IOException {
    ObjectNode result = JsonNodeFactory.instance.objectNode();
    String id = resource.getId();
    String username = mAccountService.getUsername(id);

    //Delete user likes
    QueryContext queryContext = getQueryContext(request);
    //Get ids of like resrouces
    List<String> likeIds = getLikesByProfileId(id, queryContext);
    likeIds.forEach(likeResource -> {
      try {
        deleteResource(likeResource, request);
      } catch (IOException e) {
        logger.error("Could not delete like", e);
      }
    });
    mBaseRepository.deleteResource(id, getMetadata(request));
    if (!mAccountService.deleteUser(username)) {
      logger.error("Could not delete user " + username);
    }
    result.put("message", "deleted user " + id);
    return ResponseEntity.ok(result);

  }

  /**
   * Get the ids of like resources for the given user
   */
  private List<String> getLikesByProfileId(String profileId, QueryContext queryContext) {
    logger.info("Getting likes for user {}", profileId);
    List<String> likeIds = new ArrayList<String>();
    String q = "";
    String sort = "";
    Integer from = 0;
    Integer size = -1;

    //Query like objects
    Map<String, List<String>> filters = new HashMap<>();
    filters.put("about.objectIn.@type", List.of("LikeAction"));
    filters.put("about.objectIn.agent.@id", List.of(profileId));

    String searchConfigFile = mConf.search().confFile();
    SearchConfig searchConfig = searchConfigFile != null
      ? new SearchConfig(searchConfigFile)
      : new SearchConfig();
    queryContext.setElasticsearchFieldBoosts(searchConfig.getBoostsForElasticsearch());
    ResourceList likedResources = mBaseRepository.query(q, from, size, sort, filters, queryContext);

    //For each liked resource
    likedResources.getItems().forEach(resource -> {
      ArrayNode objectIns = (ArrayNode) resource.toJson().get("about").get("objectIn");
      //Extract LikeAction authored by the given user
      List<String> likeId = StreamSupport.stream(objectIns.spliterator(), false)
        .filter(node -> node.get("@type").asText().equals("LikeAction"))
        .filter(node -> node.has("agent"))
        .filter(node -> {
          ArrayNode agents = (ArrayNode) node.get("agent");
          AtomicBoolean isAuthor = new AtomicBoolean(false);
          agents.forEach(agent -> isAuthor.set(isAuthor.get()
            || agent.get("@id").asText().equals(profileId)));
          return isAuthor.get();
        })
        .map(node -> node.get("@id").asText())
        .collect(Collectors.toList());
      likeIds.addAll(likeId);
    });
    return likeIds;
  }
}
