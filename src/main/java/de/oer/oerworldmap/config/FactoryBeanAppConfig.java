package de.oer.oerworldmap.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.oer.oerworldmap.services.AccountService;
import de.oer.oerworldmap.services.AccountServiceFactory;

@Configuration
public class FactoryBeanAppConfig {

    @Bean(name = "accountService")
    public AccountServiceFactory accountServiceFactory() {
        AccountServiceFactory factory = new AccountServiceFactory();
        return factory;
    }

    @Bean
    public AccountService accountService() throws Exception {
        return accountServiceFactory().getObject();
    }
}