package de.oer.oerworldmap.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import de.oer.oerworldmap.controllers.auth.AdminAuthorizerInterceptor;
import de.oer.oerworldmap.controllers.auth.LogAuthorizerInterceptor;
import de.oer.oerworldmap.controllers.auth.OIDCHeaderInterceptor;
import de.oer.oerworldmap.controllers.auth.ResourceAuthorizerInterceptor;
import de.oer.oerworldmap.controllers.auth.UserAuthorizerInterceptor;
import de.oer.oerworldmap.controllers.CachedInterceptor;

@Configuration
public class ControllerConfig implements WebMvcConfigurer {

  private UserAuthorizerInterceptor userAuthorizerInterceptor;
  private ResourceAuthorizerInterceptor resourceAuthorizerInterceptor;
  private OIDCHeaderInterceptor oidcHeaderInterceptor;
  private CachedInterceptor cachedInterceptor;
  private LogAuthorizerInterceptor logAuthorizerInterceptor;
  private AdminAuthorizerInterceptor adminAuthorizerInterceptor;

  @Autowired
  public ControllerConfig(UserAuthorizerInterceptor userAuthorizerInterceptor,
      ResourceAuthorizerInterceptor resourceAuthorizerInterceptor, OIDCHeaderInterceptor oidcHeaderInterceptor,
      CachedInterceptor cachedInterceptor, LogAuthorizerInterceptor logAuthorizerInterceptor,
      AdminAuthorizerInterceptor adminAuthorizerInterceptor) {
    this.userAuthorizerInterceptor = userAuthorizerInterceptor;
    this.resourceAuthorizerInterceptor = resourceAuthorizerInterceptor;
    this.oidcHeaderInterceptor = oidcHeaderInterceptor;
    this.cachedInterceptor = cachedInterceptor;
    this.logAuthorizerInterceptor = logAuthorizerInterceptor;
    this.adminAuthorizerInterceptor = adminAuthorizerInterceptor;
  }



  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(userAuthorizerInterceptor)
      .addPathPatterns("user/profile");
    registry.addInterceptor(resourceAuthorizerInterceptor)
      .addPathPatterns(
        "/resource/",
        "/resource/{*id}"
      );
    registry.addInterceptor(oidcHeaderInterceptor)
      .addPathPatterns(
        "/user/**",
        "/resource/",
        "/resource/{*id}/**",
        "/reconcile"
      );
    registry.addInterceptor(cachedInterceptor)
      .addPathPatterns(
        "/resource/",
        "/resource/{*id}"
      );
    registry.addInterceptor(logAuthorizerInterceptor)
      .addPathPatterns("/log/**");
    registry.addInterceptor(adminAuthorizerInterceptor)
      .addPathPatterns(
        "/record/**",
        "/import/**",
        "/index/**",
        "/user/groups/**");
    WebMvcConfigurer.super.addInterceptors(registry);
  }
}
