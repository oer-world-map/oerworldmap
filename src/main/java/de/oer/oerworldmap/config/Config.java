package de.oer.oerworldmap.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
public record Config(
    TdbCfg tdb, GraphCfg graph, IndexCfg index, ElasticSearchCfg es,
    KeycloakCfg keycloak, JsonCfg json, JsonLdCfg jsonld, HtConfig ht,
    ProxyCfg proxy, ReconcileCfg reconcile, SearchCfg search, UserCfg user
  ) {
  public record TdbCfg(String dir) {}
  public record GraphCfg(GraphHistoryCfg history) {
    public record GraphHistoryCfg(String dir, String file, boolean autoload) {}
  }
  public record IndexCfg(boolean async) {}
  public record ElasticSearchCfg(String hostServer, Integer hostPortHttp,
    String indexName, String indexType, String clusterName,
    String searchFuzzyness, String requestRefreshpolicy) {}
  public record KeycloakCfg(String serverUrl, String realm, String username, String password,
    String client) {}
  public record JsonCfg(String schema) {}
  public record JsonLdCfg(String context) {}
  public record HtConfig(String permissions, String apache2ctlRestart) {}
  public record ProxyCfg(String host) {}
  public record ReconcileCfg(String confFile) {}
  public record SearchCfg(String confFile) {}
  public record UserCfg(String deleteReasonLog, String onboardingTokenName) {}
}
