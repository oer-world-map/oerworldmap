package de.oer.oerworldmap.helpers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ListProcessingReport;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import de.oer.oerworldmap.models.Resource;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by fo on 15.08.17.
 */
public class JsonSchemaValidator {
  private static final Logger logger = LoggerFactory.getLogger(JsonSchemaValidator.class);

  private JsonNode mSchemaNode;
  private static final ObjectMapper mObjectMapper = new ObjectMapper();

  public JsonSchemaValidator(File aSchemaFile) throws IOException {
    mSchemaNode = mObjectMapper.readTree(aSchemaFile);
  }

  public ProcessingReport validate(Resource aResource) {
    ProcessingReport report = new ListProcessingReport();
    try {
      String type = aResource.getType();
      if (null == type) {
        report.error(new ProcessingMessage()
          .setMessage(
            "No type found for ".concat(aResource.toString()).concat(", cannot validate")));
      } else {
        JsonSchema schema = JsonSchemaFactory.byDefault()
          .getJsonSchema(mSchemaNode, "/definitions/".concat(type));
        report = schema.validate(aResource.toJson());
      }
    } catch (ProcessingException e) {
      logger.error("Error validating", e);
    }
    return report;
  }
}
