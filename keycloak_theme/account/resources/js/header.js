window.ENVIRONMENT = "production";
window.LANG = "en";
window.SUPPORTED_LANGUAGES = "en,de".split(",")

window.addEventListener('load', function () {
  // Dummy element for header injection
  var app = document.getElementById('app');
  var headerElement = document.createElement("div");
  headerElement.setAttribute('data-inject-header', null)
  document.getElementsByTagName('body')[0].insertBefore(headerElement, app)

  // add style for the header
  var style = document.createElement("link");
  style.rel="stylesheet";
  style.type="text/css";
  style.href="/oerworldmap-ui/assets/css/styles.css";
  document.head.appendChild(style);

  // add static bundle for header injection
  var script = document.createElement("script");
  script.type = "module";
  script.src = "/oerworldmap-ui/assets/js/bundle.js";
  document.head.appendChild(script);
})
