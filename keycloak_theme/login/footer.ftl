<#macro content>
    <div style="background-color:var(--pf-v5-c-login__main--BackgroundColor)">
        <hr id="kc-login-footer-divider">
        <ul id="kc-login-footer-links">
            <li><a href="${msg("imprintLink")}">${msg("imprintLinkText")}</a></li>
            <li>•</li>
            <li><a href="${msg("aboutLink")}">${msg("aboutLinkText")}</a></li>
            <li>•</li>
            <li><a href="mailto:support@oerworldmap.org">support@oerworldmap.org</a></li>
        </ul>
    <div>
</#macro>
