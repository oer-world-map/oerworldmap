#!/bin/bash

cd "$(dirname $(readlink -f $0))" || exit

until curl --output /dev/null --silent --head --fail http://localhost:9200; do
    printf '.'
    sleep 5
done
until curl --output /dev/null --silent --head --fail http://localhost:8080; do
    printf '.'
    sleep 5
done

mvn spring-boot:run
