# Open Educational Resources (OER) World Map

For initial background information about this project please refer to the
[Request for Proposals](http://www.hewlett.org/sites/default/files/OER%20mapping%20RFP_Phase%202%20Final%20June%2023%202014.pdf).

## Management documentation

 1. [User management](./docs/user-management.md)
 2. [Translations](./docs/translations.md)

## Setup project

### Get Source

```bash
git clone https://gitlab.com/oer-world-map/oerworldmap.git
```

### Adapt configuration

Either use the configuration file
```
src/main/resources/application.properties
```

Or environment variables with all upper case letters, for example:
```
KEYCLOAK_USERNAME=admin@example.com
```
See [#47](https://gitlab.com/oer-world-map/oerworldmap/-/issues/47#note_1919862370) for a full example or the docker-compose setup.

#### Docker
For docker, use the settings in `docker/.env` (template: `docker/docker.env`)

You can skip the manual installation steps and proceed with `Running for development` below.

### Setup Elasticsearch

#### [Download and install elasticsearch](https://www.elastic.co/downloads/elasticsearch)

```bash
mkdir third-party
cd third-party
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.2.1.zip
unzip elasticsearch-6.2.1.zip
cd elasticsearch-6.2.1
bin/elasticsearch-plugin install analysis-icu
bin/elasticsearch
```

Check with `curl -X GET http://localhost:9200/` if all is well.

Optionally, you may want to [use the head plugin](https://www.elastic.co/blog/running-site-plugins-with-elasticsearch-5-0).
This basically comes down to

```bash
cd .. # back to oerworldmap/third-party or choose any directory outside this project
git clone git://github.com/mobz/elasticsearch-head.git
cd elasticsearch-head
npm install
npm run start
# open http://localhost:9100/
```

#### Configure elasticsearch

If you are in an environment where your instance of elasticsearch won't be the only one on the network, you might want
to configure your cluster name to be different from the default `elasticsearch`. To do so, shut down elasticsearch and
edit `cluster.name` in `third-party/elasticsearch-2.4.1/config/elasticsearch.yml` and `es.cluster.name`
in `src/main/resources/application.properties` before restarting.

#### Create and configure oerworldmap index

```bash
curl -H "Content-type: application/json" -X PUT http://localhost:9200/oerworldmap/ -d @conf/index-config.json
```

#### If you're caught with some kind of buggy index during development, simply delete the index and re-create:

```
curl -X DELETE http://localhost:9200/oerworldmap/
curl -H "Content-type: application/json" -X PUT http://localhost:9200/oerworldmap/ -d @conf/index-config.json
```

#### Set up Keycloak

Download Keycloak 25.0.4 from https://github.com/keycloak/keycloak/releases/

You can use either the script `docker/setup_keycloak.sh` mentioned below or do the setup manually.

- Create an admin user: https://www.keycloak.org/server/bootstrap-admin-recovery
- Start the server
- Login to the admin UI at `http://localhost:8080/auth/admin/master/console` and complete the following steps:
- Create a realm called oerworldmap
- Enable "User registration", "Email as username", "Forgot password" and "Login with email" in the "Realm Settings > Login" section
- If you want to enable Email verification, configure an SMTP server in the "Realm Settings > Email" section
- Install & configure theme from `keycloak_theme/`
  - Set `oerworldmap` as "Login Theme" and "Email Theme" in the "Realm Settings > Themes" section
- To require Terms and Conditions approval:
  - Go to *Authentication*, duplicate the *registration* flow.
  - At the first step, add a step. Select *Terms and conditions*.
  - Set the requirement to *Required*
  - Bind the flow to *registration flow*
  - In *Realm Settings* > *Localization* > *Realm Overrides* you can set the T&C title, text and button with `termsTitle`, `termsText` and `acceptTerms` for your languages.
- For hcaptcha support:
  - Install the hcaptcha provider https://github.com/p08dev/keycloak-hcaptcha. In the docker-setup, the provider is already installed.
  - Go to Authentication, select the active registration flow
  - At the first step, add a step. Select *hCaptcha*.
  - Configure it and set the *Site Key* and *Secret* from your hcaptcha account
  - In *Realm Settings* > *Security Defenses* > *Content-Security-Policy* add `https://newassets.hcaptcha.com/` to `frame-src 'self'`
  - Make sure you are using the keycloak login theme, otherwise hcaptcha won't be visible in the registration page.
  - https://github.com/p08dev/keycloak-hcaptcha/pull/10
  - Go to *Realm Settings* > *User profile* and create a new attribute `h-captcha-response` and add a validator with type `length`. Set the minimum size to 0 and the maximum size to 4096.
- Alternatively, for ReCaptcha support, configure "Realm Settings > Security Defenses":
  - In *Content-Security-Policy* add `https://www.google.com` to `frame-src 'self'`
- Create and select the "oerworldmap" client ID in the "Clients" section
  - Set the "Home URL"
  - Set `*` as "Valid Redirect URIs"
  - Set `+` as "Valid post logout redirect URIs"
  - Set *Client authentication* to on
  - Go to the tab *Client scopes*, select `oerworldmap-dedicated`. In *Mappers*:
    - Use *Configure a new mapper* to search for *User Attribute* and add it
    - Edit it and set *User Attribute* and *Token Claim Name* to `profile_id`.
    - Alternatively, it is also possible to add the Profile ID in *Client Scopes*, `profile`
    - Add another mapper *From predefined mappers*, use the existing mapper `groups`, edit it and activate *Add to userinfo*.
    - Add another mapper *From predefined mappers*, use the existing mapper `realm roles` and activate *Add to userinfo*.
- In *Realm Roles*, create a new role named `admin`
- In *Groups* create a new group named `admin`
  - In *Groups*, create a group `admin`.
  - Select it and go to *Role mapping*, *Assign role* and add the newly created `admin` realm role.
- To enable self-management of users (email address, password): In *Realm Roles* select *default-roles-oerworldmap*, click the button *Assign role*, Filter by clients and add `manage-account` of the `account` client.
- To allow the members of the `admin` group/realm role some Keycloak management task, go to *Realm roles*, select `admin`, go to the tab *Associated roles* and add all the roles (permissions) you'd like them to have.
- Copy the client secret from the "Clients > `oerworldmap` > Credentials" section to `conf/httpd/vhost.conf`, parameter `OIDC_CLIENT_SECRET`
- Generate a new `OIDC_CRYPTO_PASSPHRASE` (for example with `head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32`) and set it in `conf/httpd/vhost.conf`
- In the "Authentication > Flows" section, copy the "Registration" flow and name it "Registration with Validation and Newsletter":
  - Click "Add Execution", select "Script" from the "Provider" drop down and hit save, the new script will show up at the bottom of the table
  - Set it to "REQUIRED" and select "Config" from the "Actions drop down"
  - Set the "Alias" to "Validate custom attributes"
  - Copy the script provided in `scripts/keycloakValidateCustomAttributes.js` to "Script Source"
  - Click "Add Execution", select "Script" from the "Provider" drop down and hit save, the new script will show up at the bottom of the table
  - Set it to "REQUIRED" and select "Config" from the "Actions drop down"
  - Set the "Alias" to "Subscribe newsletter"
  - Copy the script provided in `scripts/keycloakRegisterNewsletter.js` to "Script Source"
- Finally, configure the Keykloak connection in the APIs `src/main/resources/application.properties`, or set them as environment variable (all upper case and replace `.` with `_`):
  - keycloak.realm="oerworldmap"
  - keycloak.username="YOUR_KEYCLOAK_ADMIN_USER"
  - keycloak.password="YOUR_KEYCLOAK_ADMIN_PASSWORD"
  - keycloak.client="admin-cli"

#### Set up Apache

```bash
sudo apt-get install apache2 libapache2-mod-auth-openidc
sudo a2enmod xml2enc proxy proxy_html proxy_http rewrite auth_openidc ssl headers
```

If OpenIDC is not available in the package repositories, install it from source: [OpenIDC](https://github.com/OpenIDC/mod_auth_openidc/)

Configure the variables `conf/httpd/vhost.conf`.

Enable the site

```bash
sudo ln -s oerworldmap/conf/httpd/vhost{,.oerworldmap}.conf /etc/apache2/sites-available/oerworldmap.conf
sudo a2ensite oerworldmap.conf
sudo apache2ctl graceful
```

In local development environments, you can add local hostname to `/etc/hosts` for easier access:

```bash
127.0.0.1	oerworldmap.local
```

If the httpd container is also behind another reverse proxy, it is recommended to activate an configure the remoteip module in `conf/httpd/extra/remoteip.conf`:

```
LoadModule remoteip_module modules/mod_remoteip.so
RemoteIPTrustedProxy $REVERSE_PROXY_IP_ADDR
# optionally also add 172.0.0.0/8 (internal docker network) depending on your setup

RemoteIPHeader X-Forwarded-For
```

### Create database histories

```bash
mkdir -p data/consents/objects
touch data/consents/history
mkdir -p data/commits/objects/
touch data/commits/history
```

### Setup Java

Install Java SDK 23 and Maven

### Install UI

UI Components are available at https://gitlab.com/oer-world-map/oerworldmap-ui/


## Loading data

First, stop the backend

```bash
# Reset elasticsearch index
curl -X DELETE http://localhost:9200/oerworldmap/
curl -H "Content-type: application/json" -X PUT http://localhost:9200/oerworldmap/ -d @conf/index-config.json

# Delete triple store
rm data/tdb/*

# Delete history
rm -r data/commits/

# Extract history from archive
tar -xzf commits.tar.gz -C data/
```

Afterwards, start the backend again. You can follow the log output to see the progress, if debug logging is active.
Once the import finished, you can check if it's working correctly by running:
```
curl http://localhost:9000/resource/
```

### Reindexing (Elasticsearch index)

In case you need to re-index all data (for example if the index configuration changed), first stop the backend and then use the steps for loading the data like in the previous section:
* resetting the elasticsearch index
* deleting the triple store (`data/tdb`), but don't delete or reset the history (`data/commits`).
* start the backend
* The triple store and Elasticsearch index will be re-created on startup (watch log output for the progress)

## Updating vocabulary (concept values)

When updating vocabulary definitions, you need to update triple store by submitting the data to `/import`:

```bash
curl -H "Content-type: application/json" http://localhost:8080/import/ -d @ui/src/json/policyTypes.json -H 'Cookie: mod_auth_openidc_session=$SESSIONID'
```

Insert your session id (admin privileges are required) and adapt the source file.

## Contribute

### Running for development

```bash
git submodule init
git submodule update
```

If you have local changes to the vocabuary ("Concepts"), set the URL to the local ones. Otherwise you can skip this step.
For example in a default docker environment:

```bash
find conf import public -type f \( -iname \*.json -o -iname \*.conf \) -exec sed -i 's/https:\/\/oerworldmap.org/http:\/\/localhost:8080/g' {} \;
```

Check if `ui/docs` is writable, otherwise jekyll fails to start in the container.

Prepare the data directories
```bash
mkdir -p data/commits/objects/
touch data/commits/history

# Adapt the docker/.env file to your needs, by changing the hostnames and
# don't forget to specify KEYCLOAK_ADMIN and KEYCLOAK_ADMIN_PASSWORD
cp docker/docker.env docker/.env

docker-compose -f docker/docker-compose.yml -f docker/docker-compose.override.yml up
```
The httpd container will only listen on localhost by default (see `LISTEN_ADDRESS` in `docker/.env`).

For a production setup, use:
```bash
docker-compose -f docker/docker-compose.yml -f docker/production.yml up -d
```

### Additional httpd configuration
You can additional `*.conf` configuration files for Apache by placing files into `conf/httpd/extra/`.
These files will be used by Apache automatically and are not tracked in the repository.

### Initial Keycloak Setup

In the docker setup, an admin account is automatically created with the credentials from `KEYCLOAK_ADMIN` and `KEYCLOAK_ADMIN_PASSWORD` in `docker/.env`.

You can use the [docker/setup_keycloak.sh](docker/setup_keycloak.sh) script to set up the realm `oerworldmap` and an example user.
The user data can be set in [docker/oerworldmap-user.json](docker/oerworldmap_user.json) and defaults to:

- username/email: `example@user.example`
- password: `secret`

Alternatively, this step can be done manually:

- Browse to http://localhost:9001/ and log in with the admin account credentials.
- Navigate to "Add Realm"
- Import realm using the [docker/oerworldmap-realm.json](docker/oerworldmap-realm.json)
  - You need to edit the file beforehand: `oidc_secret=$(uuidgen) && sed "s/__OIDC_SECRET__/${oidc_secret}/" docker/oerworldmap-realm.json > docker/oerworldmap-realm-with-secret.json`
  - Then paste the `${oidc_secret}` into `docker/.env` after `OIDC_CLIENT_SECRET=`
- Create an example user in the new `oerworldmap` realm

#### With Docker Setup or behind a reverse proxy with SSL offloading
When running Keycloak in the Docker setup or any other SSL offloading, we can tell Keycloak not to require SSL itself, as it is already covered and ensured by the proxy.

```bash
# If running Keycloak in a docker container, enter the container
docker exec -ti docker_keycloak_1 bash
# authenticate with the admin credentials. The password is queried on stdin
cd /opt/keycloak/bin/
./kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user user
# disable SSL requirement for your realms, for example
./kcadm.sh update realms/master -s sslRequired=NONE
./kcadm.sh update realms/oerworldmap -s sslRequired=NONE
```

If you want to encrypt the communication between the proxy and Keycloak, which can be required and useful depending on your network and setup:

- create a certificate and key (a self-signed certificate may be sufficient)
- configure Keycloak to use them
- configure your proxy to contact Keycloak at port 8443

Since Keycloak 17, Keycloak no longer generates self-signed certificates automatically.

### Access

The application components are available using the following urls

- Httpd http://localhost:8080
- UI http://localhost:3000
- Backend http://localhost:9000/
- Keycloak http://localhost:9001/
  - To administer keycloak, browse to http://localhost:9001/ and log in with the admin account credentials.

### Application logs

On development setups, only a single log file is kept, and only in the container.

On production setups, log files are rotated (7 archives are kept) and are mapped to the `logs/` directory.
[Spring Documentation of Rotation](https://docs.spring.io/spring-boot/reference/features/logging.html#features.logging.file-rotation)

The maximum size of a single log file before it is archived can be configured in `.env` with `SCALA_LOGFILE_SIZE`.

### Coding conventions

Indent blocks by *two spaces* and wrap lines at *100 characters*. For more
details, refer to the [Google Java Style
Guide](https://google-styleguide.googlecode.com/svn/trunk/javaguide.html).

### Bug reports

Please file bugs as an issue labeled "Bug" [here](https://gitlab.com/oer-world-map/oerworldmap/-/issues/new). Include browser information and screenshot(s) when applicable.
