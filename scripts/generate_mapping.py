#!/usr/bin/python3

from json import (
    load as json_load,
    dump as json_dump,
    dumps as json_dumps,
    loads as json_loads,
)
from re import sub as re_sub, MULTILINE as re_MULTILINE

schema = json_load(open("public/json/schema.json"))
template = json_loads(re_sub("^//.*", "", open("scripts/index_template.json").read(), flags=re_MULTILINE))


def map_types(fieldname, properties, level: int):
    print("\t" * level, f"map_types fieldname: {fieldname}, properties: {json_dumps(properties)[:150]}")
    try:
        return FIELD_MAPPING[fieldname]
    except KeyError:
        pass
    if "$ref" in properties:
        if (
            properties["$ref"].startswith("#/definitions/")
            and not properties["$ref"] in REF_MAPPING
        ):
            return map_types(fieldname, schema["definitions"][properties["$ref"][14:]], level+1)
        return REF_MAPPING[properties["$ref"]]
    elif "type" in properties:
        if properties["type"] == "object" and "properties" in properties:
            return {
                "properties": {
                    key: map_types(key, props, level+1)
                    for key, props in properties["properties"].items()
                }
            }
        elif (properties["type"] == "object" and "allOf" in properties):  # no properties, but 'allOf'
            for item in properties["allOf"]:
                if "#ref" in item:
                    # print("\t\t\t\tFallback to default linked data with type == object, #ref in allOf")
                    return LINKED_RESOURCE_TYPE_MAPPING
                elif "properties" in item:
                    # print("\t\t\t\tFallback to default linked data with type == object, properties in allOf")
                    # Only LikeAction.object and LighthouseAction.object
                    return LINKED_RESOURCE_TYPE_MAPPING
        elif properties["type"] == "array":
            if "allOf" in properties["items"]:
                for item in properties["items"]["allOf"]:
                    if "properties" in item:
                        if item["properties"]["@type"]["enum"] == ["Concept"]:
                            return CONCEPT
                        elif item["properties"]["@type"]["enum"] == ["LikeAction", "LighthouseAction"]:
                            return LINKED_ACTION_TYPE_MAPPING
                        else:
                            # print("\t\t\t\tFallback to default linked data with type == array, properties in allOf, type not enum and not Concept or Action")
                            return LINKED_RESOURCE_TYPE_MAPPING
                else:
                    raise NotImplementedError(properties)
            else:
                return map_types(fieldname, properties["items"], level+1)
        return TYPE_MAPPING[properties["type"]]
    elif 'allOf' in properties:  # not an array, just a link
        for item in properties["allOf"]:
            if "properties" in item:
                if item["properties"]["@type"]["enum"] == ["Concept"]:
                    return CONCEPT
                else:
                    # print("\t\t\t\tFallback to default linked data with type == array, properties in allOf, type not enum and not Concept or Action")
                    return LINKED_RESOURCE_TYPE_MAPPING
        else:
            raise NotImplementedError(properties)
    raise NotImplementedError(properties)


FIELD_MAPPING = {
    "@id": {"type": "keyword"},
    "@type": {"type": "keyword"},
    "@context": {"type": "keyword"},
}
REF_MAPPING = {
    "#/definitions/iso-8601-date": {"type": "date", "format": "date"},
    "#/definitions/iso-8601-datetime": {"type": "date", "format": "dateOptionalTime"},
    "#/definitions/GeoCoordinates": {"type": "geo_point"},
    "#/definitions/LocalizedString": {
        "properties": {
            "de": {
                "fields": {
                    "sort": {"type": "keyword"},
                    "simple_tokenized": {
                        "search_analyzer": "standard",
                        "type": "text",
                        "analyzer": "simple",
                    },
                    "variations": {
                        "type": "text",
                        "analyzer": "title_analyzer",
                    },
                    "splits": {
                        "search_analyzer": "standard",
                        "type": "text",
                        "analyzer": "split_analyzer",
                    },
                },
                "type": "text",
                "analyzer": "german_analyzer",
            },
            "en": {
                "fields": {
                    "sort": {"type": "keyword"},
                    "simple_tokenized": {
                        "search_analyzer": "standard",
                        "type": "text",
                        "analyzer": "simple",
                    },
                    "variations": {
                        "type": "text",
                        "analyzer": "title_analyzer",
                    },
                    "splits": {
                        "search_analyzer": "standard",
                        "type": "text",
                        "analyzer": "split_analyzer",
                    },
                },
                "type": "text",
                "analyzer": "english_analyzer",
            },
        }
    },
}

LINKED_RESOURCE_TYPE_MAPPING = {
    "properties": {
        "@id": {"type": "keyword"},
        "@type": {"type": "keyword"},
        "@context": {"type": "keyword"},
        "name": REF_MAPPING["#/definitions/LocalizedString"],
        "hashtag": {"type": "keyword"},
        "alternateName": REF_MAPPING["#/definitions/LocalizedString"],
    }
}
LINKED_ACTION_TYPE_MAPPING = {
    "properties": {
        "@id": {"type": "keyword"},
        "@type": {"type": "keyword"},
        "@context": {"type": "keyword"},
        "agent": {
            "properties": {
                "@id": {"type": "keyword"},
                "@type": {"type": "keyword"},
            }
        }
    }
}
TYPE_MAPPING = {
    "string": {"type": "keyword"},
    "boolean": {"type": "boolean"},
}
ID_TYPE_NAME = {
    "properties": {
        "@id": {"type": "keyword"},
        "@type": {"type": "keyword"},
        "name": REF_MAPPING["#/definitions/LocalizedString"],
    }
}
CONCEPT = ID_TYPE_NAME.copy()

webpage_properties = {}


def properties_from_type(schema_type):
    print(f"\tproperties_from_type")
    index_type = {}
    for fieldname, properties in schema_type.items():
        print(f"\t\tfieldname {fieldname}")
        index_type[fieldname] = map_types(fieldname, properties, 3)
    return index_type

def update_merge(d1, d2):
    # CC-BY-SA Olivier Melançon https://stackoverflow.com/a/50441142/2851664
    if isinstance(d1, dict) and isinstance(d2, dict):
        # Unwrap d1 and d2 in new dictionary to keep non-shared keys with **d1, **d2
        # Next unwrap a dict that treats shared keys
        # If two keys have an equal value, we take that value as new value
        # If the values are not equal, we recursively merge them
        return {
            **d1, **d2,
            **{k: d1[k] if d1[k] == d2[k] else update_merge(d1[k], d2[k])
            for k in {*d1} & {*d2}}
        }
    else:
        # This case happens when values are merged
        # It bundle values in a list, making sure
        # to flatten them if they are already lists
        return [
            *(d1 if isinstance(d1, list) else [d1]),
            *(d2 if isinstance(d2, list) else [d2])
        ]


for resource_type in [resource_type["$ref"] for resource_type in schema["oneOf"]] + ["Comment"]:
    resource_type = resource_type[resource_type.rfind("/") + 1 :]
    if resource_type == "Concept":
        # FIXME
        print("Skipping Concept")
        # continue
    print(f"Handle resource type {resource_type}")
    # merge recursively
    webpage_properties = update_merge(webpage_properties, properties_from_type(
        schema["definitions"][resource_type]["properties"]))

template["mappings"]["WebPage"]["properties"]["about"]["properties"] = webpage_properties

with open("conf/index-config.json", "w") as handle:
    json_dump(template, handle, indent="  ")
