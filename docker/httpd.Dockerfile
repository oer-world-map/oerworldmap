ARG MOD_AUTH_OPENIDC_VERSION=2.4.15.3
ARG DEBIAN_CODENAME=bookworm
ARG HTTPD_VERSION=2.4.58
FROM httpd:${HTTPD_VERSION}-${DEBIAN_CODENAME}

ARG MOD_AUTH_OPENIDC_VERSION
ARG DEBIAN_CODENAME

RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && apt-get install -y \
        wget \
        libcjose0 libhiredis0.14 apache2-bin less

RUN wget https://github.com/OpenIDC/mod_auth_openidc/releases/download/v${MOD_AUTH_OPENIDC_VERSION}/libapache2-mod-auth-openidc_${MOD_AUTH_OPENIDC_VERSION}-1.${DEBIAN_CODENAME}_amd64.deb
RUN dpkg -i libapache2-mod-auth-openidc_${MOD_AUTH_OPENIDC_VERSION}-1.${DEBIAN_CODENAME}_amd64.deb

RUN sed -i "$ a Include /srv/oerworldmap_conf/vhost.docker.conf" /usr/local/apache2/conf/httpd.conf

RUN mkdir /var/log/apache2

WORKDIR /usr/local/apache2/

CMD ["httpd-foreground", "-DHTTPD_X_FORWARDED_HEADERS"]
