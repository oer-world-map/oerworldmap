#!/bin/bash

set -a  # also export the variables
[ -f ../.env ] && source ../.env
set +a

if [[ "$JEKYLL_ENV" == "production" ]]; then
    jekyll build
else
    jekyll build --watch --incremental
fi
