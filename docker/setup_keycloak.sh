#!/bin/bash

[ -n "${DEBUG-}" ] && set -x
set -euo pipefail

fail_with_msg () {
    echo "$@"
    exit 1
}

require_program () {
    which "$1" > /dev/null || fail_with_msg "Please install '$1'."
}

require_program uuidgen
require_program jq
require_program curl

DIR=$(dirname -- "$0")
CURL="curl --silent --fail-with-body"

# read environment variables from .env
. $DIR/.env

KEYCLOAK_URL=${KEYCLOAK_URL:-http://localhost:9001/auth}
KEYCLOAK_REALM=${KEYCLOAK_REALM:-master}
[ -n "${KEYCLOAK_ADMIN-}" ] || fail_with_msg "You need to set KEYCLOAK_ADMIN in $DIR/.env"
[ -n "${KEYCLOAK_ADMIN_PASSWORD-}" ] || fail_with_msg "You need to set KEYCLOAK_ADMIN_PASSWORD in $DIR/.env"
OER_REALM=${OER_REALM:-oerworldmap}

oidc_secret=$(uuidgen -r)
oer_realm_data=$(sed "s/__OIDC_SECRET__/${oidc_secret}/" ${DIR}/oerworldmap-realm.json)

counter=0
max_try=${MAX_TRY:-10}
TKN="null"
while [[ ("$TKN" = "null" || "$TKN" = "") && "$counter" != "$max_try" ]]
do
    counter=$((counter+1))
    echo "Trying to get access token..."
    TKN_EXIT=0
    TKN_CURL=$($CURL -X POST "${KEYCLOAK_URL}/realms/${KEYCLOAK_REALM}/protocol/openid-connect/token" \
        -H "Content-Type: application/x-www-form-urlencoded" \
        -d "username=${KEYCLOAK_ADMIN}" \
        -d "password=${KEYCLOAK_ADMIN_PASSWORD}" \
        -d 'grant_type=password' \
        -d 'client_id=admin-cli') \
        || TKN_EXIT=$?
    if [ "$TKN_EXIT" -ne 0 ]; then
        sleep 2
        continue
    fi
    TKN=$(echo "$TKN_CURL" | jq -r '.access_token')
    sleep 2
done
if [[ "$TKN_EXIT" -ne 0 || "$TKN" = "null"  || "$TKN" = "" ]]; then
    printf "Error retrieving token. Curl exit code: %s. Raw output:\n%s\n" "$TKN_EXIT" "$TKN_CURL"
    exit $TKN_EXIT
fi

echo "Adding realm ${OER_REALM}"
REALM_EXIT=0
REALM_CURL=$($CURL -X POST "${KEYCLOAK_URL}/admin/realms" \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer $TKN" \
    -d "$oer_realm_data") \
    || REALM_EXIT=$?
if [[ "$REALM_EXIT" -ne 0 ]]; then
    printf "Error adding realm. Curl exit code: %s. Raw output:\n%s\n" "$REALM_EXIT" "$REALM_CURL"
    exit $REALM_EXIT
fi

echo "OIDC Client Secret is $oidc_secret. Add this to docker/.env:"
echo "OIDC_CLIENT_SECRET=$oidc_secret"

echo "Creating example user"
USER_EXIT=0
USER_CURL=$($CURL -X POST "${KEYCLOAK_URL}/admin/realms/$OER_REALM/users" \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer $TKN" \
    -d @${DIR}/oerworldmap-user.json) \
    || USER_EXIT=$?
if [[ "$USER_EXIT" -ne 0 ]]; then
    printf "Error creating user. Curl exit code: %s. Raw output:\n%s\n" "$USER_EXIT" "$USER_CURL"
    exit $USER_EXIT
fi
