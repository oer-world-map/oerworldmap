#!/bin/bash

#Clean tdb lock file
rm -f data/tdb/tdb.lock

#Run backend
if [ -z "${DEBUG_SCALA-}" ]; then
mvn spring-boot:run
else
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=*:5005"
fi
