FROM node:20.10.0-bullseye as ui-base
WORKDIR /srv/oerworldmap-ui

ARG NODE_ENV=development

# required for the build:prod step below, otherwise assets have an undefined URL
ARG PUBLIC_URL
ENV PUBLIC_URL=$PUBLIC_URL

COPY ui/ /srv/oerworldmap-ui/
COPY docker/entrypoint-ui.sh /srv

RUN npm install -f

ENTRYPOINT [ "/srv/entrypoint-ui.sh" ]

FROM ui-base as ui-prod

RUN npm run build:prod
