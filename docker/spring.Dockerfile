ARG JDK_VERSION=23
ARG DEBIAN_CODENAME=bookworm
FROM openjdk:${JDK_VERSION}-slim-${DEBIAN_CODENAME} AS spring-base

RUN apt update && DEBIAN_FRONTEND="noninteractive" apt install -y maven && apt clean

WORKDIR /srv/oerworldmap

ENTRYPOINT [ "/srv/oerworldmap/docker/entrypoint-spring.sh" ]

FROM spring-base as spring-prod

COPY src/ /srv/oerworldmap/src
COPY conf/ /srv/oerworldmap/conf
COPY pom.xml /srv/oerworldmap/

RUN mvn -DskipTests=true package
