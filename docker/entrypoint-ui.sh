#!/bin/bash

set -eu -o pipefail

if [ "$NODE_ENV" = "production" ]; then
    npm run server:prod
else
    echo "BUILD DEV"
    npm run build:dev &
    echo "SERVER DEV"
    npm run server:dev

    #WORKAROUND so the docker container doesn't exit and no server is running.
    while true; do sleep 1000; done
fi
