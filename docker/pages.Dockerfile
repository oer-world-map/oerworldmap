FROM jekyll/jekyll:4.2.0

RUN gem install jekyll-environment-variables

COPY docker/entrypoint-pages.sh /srv/

WORKDIR /srv/jekyll

ENTRYPOINT [ "/srv/entrypoint-pages.sh" ]
