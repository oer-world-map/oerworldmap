# keycloak 25 uses java 21
FROM openjdk:21-slim-bookworm AS hcaptcha-builder

RUN apt update && DEBIAN_FRONTEND="noninteractive" apt install -y git maven

RUN git clone https://github.com/p08dev/keycloak-hcaptcha.git /opt/keycloak-hcaptcha
WORKDIR /opt/keycloak-hcaptcha
RUN mvn clean compile package

FROM quay.io/keycloak/keycloak:26.0.7 AS builder

COPY --from=hcaptcha-builder /opt/keycloak-hcaptcha/target/keycloak-hcaptcha.jar /opt/keycloak/providers/

# Enable health and metrics support
#ENV KC_HEALTH_ENABLED=true
#ENV KC_METRICS_ENABLED=true

# Configure a database vendor
ENV KC_DB=postgres

WORKDIR /opt/keycloak
# for demonstration purposes only, please make sure to use proper certificates in production instead
# RUN keytool -genkeypair -storepass password -storetype PKCS12 -keyalg RSA -keysize 2048 -dname "CN=server" -alias server -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keystore conf/server.keystore
RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak:26.0.7
COPY --from=builder /opt/keycloak/ /opt/keycloak/
