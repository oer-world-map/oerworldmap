Define API_HOST http://scala:9000
Define UI_HOST http://ui:80
Define ELASTICSEARCH_HOST http://elasticsearch:9200
Define KEYCLOAK_HOST http://keycloak:8080
Define APACHE_LOG_DIR /var/log/apache2/
Define ERROR_LOG ${APACHE_LOG_DIR}/error.log
Define CUSTOM_LOG ${APACHE_LOG_DIR}/access.log
Define ASSETS_DIR /srv/oerworldmap_assets/

LoadModule auth_openidc_module /usr/lib/apache2/modules/mod_auth_openidc.so
# mod_xml2enc required for mod_proxy_html to prevent "AH01425: I18n support in mod_proxy_html requires mod_xml2enc. Without it, non-ASCII characters in proxied pages are likely to display incorrectly.", see https://gitlab.com/oer-world-map/oerworldmap/-/issues/18
LoadModule xml2enc_module modules/mod_xml2enc.so
LoadModule proxy_html_module modules/mod_proxy_html.so
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_http_module modules/mod_proxy_http.so
LoadModule rewrite_module modules/mod_rewrite.so

ServerName ${PUBLIC_HOSTNAME}
Listen ${LISTEN_PORT}

DocumentRoot /srv/oerworldmap_docs/
<Directory /srv/oerworldmap_docs/>
  Require all granted
</Directory>
<Directory ${ASSETS_DIR}>
  Require all granted
</Directory>

<VirtualHost *:${LISTEN_PORT}>

  # Server
  ServerName ${PUBLIC_HOSTNAME}
  ServerAdmin ${PUBLIC_EMAIL}

  AllowEncodedSlashes NoDecode
  ProxyPreserveHost on
  <IfDefine HTTPD_X_FORWARDED_HEADERS>
    # if Apache/the httpd container is running behind a reverse proxy with SSL/TLS offloading, activate this option
    # see https://github.com/OpenIDC/mod_auth_openidc/wiki#8-how-do-i-run-mod_auth_openidc-behind-a-reverse-proxy
    OIDCXForwardedHeaders ${HTTPD_X_FORWARDED_HEADERS}
  </IfDefine>

  Include /srv/oerworldmap_conf/vhost.oerworldmap.conf
  IncludeOptional /srv/oerworldmap_conf/extra/*.conf
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
