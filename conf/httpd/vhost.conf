# Vars
Define PUBLIC_HOSTNAME worldmap.example
Define LISTEN_PORT 80
Define PUBLIC_SCHEME http
Define PUBLIC_EMAIL support@worldmap.example
Define PUBLIC_URL http://worldmap.example:8080
Define API_HOST http://localhost:9000
Define UI_HOST http://localhost:3000
Define ELASTICSEARCH_HOST http://localhost:9200
Define KEYCLOAK_HOST http://localhost:8080
Define OIDC_CRYPTO_PASSPHRASE FIXME
Define OIDC_CLIENT_ID oerworldmap
Define OIDC_CLIENT_SECRET FIXME
#Define SSL_CIPHER_SUITE
#Define SSL_CERT_FILE
#Define SSL_CERT_KEY_FILE
#Define SSL_CERT_CHAIN_FILE
Define APACHE_LOG_DIR /var/log/apache2/
Define ERROR_LOG ${APACHE_LOG_DIR}/error.log
Define CUSTOM_LOG ${APACHE_LOG_DIR}/access.log
Define ASSETS_DIR /srv/oerworldmap_assets/

LoadModule auth_openidc_module /usr/lib/apache2/modules/mod_auth_openidc.so
# mod_xml2enc required for mod_proxy_html to prevent "AH01425: I18n support in mod_proxy_html requires mod_xml2enc. Without it, non-ASCII characters in proxied pages are likely to display incorrectly.", see https://gitlab.com/oer-world-map/oerworldmap/-/issues/18
LoadModule xml2enc_module modules/mod_xml2enc.so
LoadModule proxy_html_module modules/mod_proxy_html.so
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_http_module modules/mod_proxy_http.so
LoadModule rewrite_module modules/mod_rewrite.so
LoadModule ssl_module modules/mod_ssl.so

ServerName ${PUBLIC_HOSTNAME}
Listen ${LISTEN_PORT}
<VirtualHost *:${LISTEN_PORT}>

  # Server
  ServerName ${PUBLIC_HOSTNAME}
  ServerAdmin ${PUBLIC_EMAIL}

  AllowEncodedSlashes NoDecode
  ProxyPreserveHost on
  # if Apache/the httpd container is running behind a reverse proxy with SSL/TLS offloading, activate this option
  # see https://github.com/OpenIDC/mod_auth_openidc/wiki#8-how-do-i-run-mod_auth_openidc-behind-a-reverse-proxy
  #OIDCXForwardedHeaders X-Forwarded-Proto

  # SSL
  <IfDefine SSL_CERT_FILE>
    SSLEngine on
    SSLProtocol ALL -SSLv3 -SSLv3
    SSLCipherSuite ${SSL_CIPHER_SUITE}
    SSLCertificateFile ${SSL_CERT_FILE}
    SSLCertificateKeyFile ${SSL_CERT_KEY_FILE}
    SSLCertificateChainFile ${SSL_CERT_CHAIN_FILE}
    RequestHeader set X-Forwarded-Proto "https"
  </IfDefine>

  Include vhost.oerworldmap.conf
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
