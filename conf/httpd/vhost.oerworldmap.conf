# Modules
KeepAlive Off
RewriteEngine On

# CORS
Header always set Access-Control-Allow-Origin ${PUBLIC_URL}
Header always set Access-Control-Allow-Methods "PUT, GET, POST, DELETE, OPTIONS"
#Header always set Access-Control-Allow-Credentials true

# Authorization
OIDCProviderMetadataURL ${KEYCLOAK_HOST}/auth/realms/oerworldmap/.well-known/openid-configuration
OIDCRedirectURI ${PUBLIC_URL}/oauth2callback
OIDCRedirectURLsAllowed https?://${PUBLIC_HOSTNAME}/ ${PUBLIC_URL}
OIDCCryptoPassphrase ${OIDC_CRYPTO_PASSPHRASE}
OIDCClientID ${OIDC_CLIENT_ID}
OIDCClientSecret ${OIDC_CLIENT_SECRET}
OIDCProviderTokenEndpointAuth client_secret_basic
OIDCRemoteUserClaim email
OIDCScope "openid email"
# Time to live in seconds for state parameter i.e. the interval in which the authorization request
# and the corresponding response need to be processed.
OIDCStateTimeout ${OIDC_STATE_TIMEOUT}
# Maximum duration of the application session
# When not defined the default is 8 hours (3600 * 8 seconds).
# When set to 0, the session duration will be set equal to the expiry time of the ID token.
# NB: this can be overridden on a per-OP basis in the .conf file using the key: session_max_duration
OIDCSessionMaxDuration ${OIDC_SESSION_MAX_DURATION}
# Defines a default URL to be used in case of 3rd-party-init-SSO when no explicit target_link_uri
# has been provided. The user is also redirected to this URL in case an invalid authorization
# response was received.
# The default is to not redirect the browser to any URL but return an HTTP/HTML error to the user.
OIDCDefaultURL /.login?continue=/user/profile
# Interval in seconds after which the session will be invalidated when no interaction has occurred.
# When not defined, the default is 300 seconds.
OIDCSessionInactivityTimeout ${OIDC_SESSION_INACTIVITY_TIMEOUT}
ErrorDocument 401 "<script>window.location=\"/.login?continue=/user/profile\"</script>"
RewriteRule ^/auth/((admin|js|realms|resources)/.*) ${KEYCLOAK_HOST}/auth/$1 [P,L]
<Location />
    AuthType openid-connect
    Require all granted
</Location>
<Location /oauth2callback>
    Require valid-user
</Location>
<Location /.login>
    Require valid-user
</Location>
<Location /resource/>
    <If "%{QUERY_STRING} =~ /add=/">
        Require valid-user
    </If>
    <LimitExcept GET OPTIONS>
        Require valid-user
    </LimitExcept>
</Location>
<Location /resource/*/comment>
    Require valid-user
</Location>
<Location /resource/*/like>
    Require valid-user
</Location>
<Location /log/>
    Require claim groups:editor
    Require claim groups:admin
</Location>
<Location /import/>
    Require valid-user
    Require claim groups:admin
</Location>
<Location /index/>
    Require valid-user
    Require claim groups:admin
</Location>
<Location /user/profile>
    <LimitExcept OPTIONS>
        Require valid-user
    </LimitExcept>
    OIDCUnAuthAction 401
</Location>

# Basic redirects
Redirect 301 /resource /resource/
Redirect 301 /country /resource/
Redirect 301 /country/ /resource/
Redirect 301 /activity /activity/
Redirect 301 /feed /feed/
Redirect 301 /log /log/

# API Proxy
RewriteCond %{HTTP:Accept} application/json [OR]
# export of lists resources like /resource/?ext=geojson&size=-1
RewriteCond %{QUERY_STRING} ext=
RewriteRule ^((/resource/(urn:uuid:[0-9a-f-]{36})?|/user|/feed|/country|/log|/index|/label|/reconcile|/activity).*) ${API_HOST}$1 [P,L]
RewriteRule ^((/reconcile|/label).*) ${API_HOST}$1 [P,L]
# export single resources like /resource/$ID?version=HEAD&extension=csv&disposition=attachment
RewriteCond %{QUERY_STRING} extension=
RewriteCond %{QUERY_STRING} disposition=attachment
RewriteRule ^(/resource/urn:uuid:[0-9a-f-]{36})$ ${API_HOST}$1 [P,L]
RewriteRule ^(/import/.*) ${API_HOST}$1 [P,L]

# Assets Proxy / Rewrite
RewriteRule ^/robots.txt ${API_HOST}/robots.txt [P,L]
RewriteRule ^(/assets/json/)(.*) "${ASSETS_DIR}/json/$2" [L]
RewriteRule ^(/assets.*) "/$1" [L]
RewriteRule ^/oerworldmap-ui/(.*) "/$1" [L]

# Elasticsearch Proxy
RewriteRule ^/elastic/oerworldmap/_msearch(.*) ${ELASTICSEARCH_HOST}/oerworldmap/_msearch$1 [P,L]
RewriteRule ^/elastic/oerworldmap/_search(.*) ${ELASTICSEARCH_HOST}/oerworldmap/_search$1 [P,L]
RewriteRule ^/elastic/oerworldmap/_mapping(.*) ${ELASTICSEARCH_HOST}/oerworldmap/_mapping$1 [P,L]

# UI Proxy
RewriteRule ^(/.+/.*) ${UI_HOST}$1 [P,L]
RewriteRule ^/(stats.*) ${UI_HOST}/$1 [P,L]
RewriteRule ^/.login ${UI_HOST}/.login [P,L]
RewriteRule ^/__webpack_hmr ${UI_HOST}/__webpack_hmr [P,L]

# Static Pages
# the language parameter has the highest priority
RewriteCond %{QUERY_STRING} (^|&)language=(de)($|&)
RewriteRule ^((/index|/contribute|/about|/FAQ|/editorsFAQ|/imprint|/api|/oerpolicies|/aggregation|/reset|/delete).*) "/$1.%2.html" [L]
RewriteCond %{QUERY_STRING} (^|&)language=(en)($|&)
RewriteRule ^((/index|/contribute|/about|/FAQ|/editorsFAQ|/imprint|/api|/oerpolicies|/aggregation|/reset|/delete).*) "/$1.html" [L]
# index with DE header and parameter
RewriteCond %{QUERY_STRING} (^|&)language=(de)($|&)
RewriteRule ^/$ "/index.de.html" [L]
RewriteCond %{HTTP:Accept-Language} ^(de) [NC]
RewriteRule ^/$ "/index.de.html" [L]
# default index
RewriteRule ^/$ "/index.html" [L]
# fallback to header
RewriteCond %{HTTP:Accept-Language} ^(de) [NC]
RewriteRule ^((/index|/contribute|/about|/FAQ|/editorsFAQ|/imprint|/api|/oerpolicies|/aggregation|/reset|/delete).*) "/$1.%1.html" [L]
# default for no explicit language parameter or header
RewriteCond %{QUERY_STRING} !language=(de)
RewriteRule ^((/index|/contribute|/about|/FAQ|/editorsFAQ|/imprint|/api|/oerpolicies|/aggregation|/reset|/delete).*) "/$1.html" [L]
RewriteCond %{HTTP:Accept-Language} !^(de) [NC]
RewriteRule ^((/index|/contribute|/about|/FAQ|/editorsFAQ|/imprint|/api|/oerpolicies|/aggregation|/reset|/delete).*) "/$1.html" [L]

RewriteRule ^/[^/] "/404.html" [L]
ErrorDocument 404 /404.html

# Logging
<IfDefine ERROR_LOG>
    ErrorLog ${ERROR_LOG}
</IfDefine>
<IfDefine CUSTOM_LOG>
    CustomLog ${CUSTOM_LOG} combined
</IfDefine>
