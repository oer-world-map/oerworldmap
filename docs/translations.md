# Application Translation

## Adding new languages

### The language code

The application and it's current translations use two-character abbreviations for the languages, for example `en` and `de`.
The schema is known as [Alpha-2 Code](https://en.wikipedia.org/wiki/List_of_ISO_639_language_codes).
For consistency, it is recommended to use the same naming schema for new languages.

In the next sections, `$lang` is used as placeholder.

### Authentication

The languages, and their abbreviations, currently supported by Keycloak can be derived from these file names:
[Keycloak languages](https://github.com/keycloak/keycloak/tree/main/themes/src/main/resources-community/theme/base/account/messages).

We only adapt the existing translation for this component, we do not need to do all the translation here.

#### In the code repository

1. Create `keycloak_theme/email/messages/messages_$lang.properties` equivalent to the other files present in the same directory and fill it with the translated values.
2. Add the language code to the `locales` setting in `keycloak_theme/email/theme.properties`.

Commit the changes, and push them to the repository and deploy them at the installation. This requires a restart of Keycloak.

#### In the Keycloak application

1. Go to the Administration console and login as administrator: <https://oerworldmap.org/auth/admin/oerworldmap/console/>
2. Navigate to *Realm settings* and open the tab *Localization*.
3. At *Supported locales* add the new locale from the list and save the change.
4. Inside the *Localization* tab, switch to *Realm overrides*.
   A small number of customized texts are shown. Use the language switcher to change the language view and add the same texts in the new language.

#### Keycloak Account Console Header

In `keycloak_theme/account/resources/js/header.js` add the new language, equivalent to `ui/.env`, see below.

### Backend

Add translations in these files:

- `public/json/activities.json`
- `public/json/esc.json`
- `public/json/isced-1997.json`
- `public/json/licenses.json`
- `public/json/sectors.json`

Commit the change and push them to the repository.
Pull the changes at the instance and restart the backend.

### Frontend

The most work needs to be done in the code repository of the frontend (`ui`).

For clarity, all filenames here are prefixed with `ui`. Inside the ui repository, the `ui` prefix needs to be left out.

Create these files with translations of the original files:

- `ui/docs/_data/locale/descriptions_$lang.json`
- `ui/docs/_data/locale/iso3166-1-alpha-2_$lang.json`
- `ui/docs/_data/locale/iso3166-2_$lang.json`
- `ui/docs/_data/locale/iso639-1_$lang.json`
- `ui/docs/_data/locale/labels_$lang.json`
- `ui/docs/_data/locale/ui_$lang.json`

And add translations in these files ("vocabulary"/"concepts"):

- `ui/src/json/activities.json`
- `ui/src/json/esc.json`
- `ui/src/json/isced-1997.json`
- `ui/src/json/licenses.json`
- `ui/src/json/organizations.json`
- `ui/src/json/persons.json`
- `ui/src/json/policies.json`
- `ui/src/json/policyTypes.json`
- `ui/src/json/projects.json`
- `ui/src/json/sectors.json`
- `ui/src/json/services.json`

Commit the change and push them to the repository.

#### Activate the changes

1. Add the language code to the setting `LANG_SUPPORTED` in `ui/.env.example` and commit the change and push it to the repository.
2. Finally, add it to `LANG_SUPPORTED` in your production's `.env` file.
3. At the instance, pull the changes from the repository and rebuild/restart the UI (depending on your setup).

#### Import vocabulary

The vocabulary data needs to be imported to the database too.
Follow the [instructions in the README](https://gitlab.com/oer-world-map/oerworldmap/#updating-vocabulary-concept-values).

### Static pages

1. Follow `ui/docs/README.md`, section *Production* steps to rebuild the JS bundles and commit the changes.
2. In `ui/docs`, for every `.md` and `.html` file create translated copies `_$lang.md`/`_$lang.html`, equivalent to the existing translations.
3. Add the new language code to the `supported_languages` variable in `ui/docs/_includes/i18n.html`.
4. Commit the changes and and push them to the repository.
5. Pull the changes at the instance and rebuild or restart the jekyll setup, depending on the installation variant.
