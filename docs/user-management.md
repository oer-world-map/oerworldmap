# User Account Management

Login at <https://oerworldmap.org/auth/admin/oerworldmap/console/> with your user account (the user account needs to have admin privileges).

## User search
In the left menu, select "Users".

The simple search form is very basic and only shows if the search string matches the beginning of a user's email address.
To get all results, prepend an asterisk `*`, for example `*.edu` or `*fred`.

### Attribute search

For specialized searches, switch to the so-called "Attribute Search" by clicking on "Default Search" and then select "Attribute Search".
Click on "Select Attributes". At "Key," click on the input field "Select attribute" and select, for example, "Email." In the value field, enter the search value, for example, ".org." Confirm with the ✓ button and click the blue "Search" button.
Adding a wildcard `*` to these searches is not required.

![Attribute Search](./screenshots/user-management-attribute-search.png)

## Modifying users

The panel to change a user account opens after selecting a user in the search list.

The switch to disable/enable a user account is in the top right corner. It can be used to quickly deactivate a potential spam account before deleting it finally.

* ID: This is the internal ID of the user account and usually also the ID of the Personal Profile ID in the OER World Map (more details on this below).
* Email verified: Users with unverified emails must still verify their account before logging in.
* Email: To change the email address of a user account, set the new address in this field. The username is identical to the email address. Therefore, the username field is disabled.

### User Attributes

In the tab "Attributes", users can have the Profile ID an potentially onboarding attributes.

#### Profile ID

The attribute `profile_id` is the resource ID of the user account's public Personal Profile in the OER World Map. If it is not set, the user does not have a public Personal Profile.

Suppose a user previously had a user account, including a public Personal Profile, and later created a new user account (because the used email address is no longer in use). In that case, the new user account can be linked to the old public Personal Profile by setting the profile ID here.

#### Onboarding Attributes

All old user accounts have two attributes for the onboarding process, `onboarding_token` and `onboarding_email_sent`. They are only used for the onboarding. If the users click on the delete or reset links of the onboarding emails, these attributes are required for a successful completion.

### Credentials (Password)

In the credentials tab, users' passwords can be set directly with a click on the "Reset password" button.

## Managing the Admin Group

User accounts that are members of the group `admin` give them the ability to
 * delete resources
 * edit others' personal Profiles
 * view the edit history of entries
 * change the type of resources and
 * manage user accounts (as documented here)
 
The memberships can be managed in two ways:

 1. Select *Groups* from the left menu and then select *admin*. In the tab *Members*, all current members are listed. Remove members by ticking the checkbox next to the user, then the three-dot link left of *Refresh* and *Leave*. Promote a user to admin with the blue *Add member* button.
 
    ![Remove users from admin group](./screenshots/user-management-groups-leave.png)
 2. Select a user (see above) and switch to the *Groups* tab. Press *Join Group* and select the group `admin` to add the user to the admins. Or, if the user is already a member, tick the checkbox next to the `admin` entry in the list and then click *Leave* above.

## Events

The events view lists all activities of users in regard to the user account (login, logout, password reset, changed email addresses) in the last seven days. It can be helpful for user support.

The function *Search user event* allows filtering by user by entering the user's ID (the 36 alphanumeric characters).
