# OER World Map API documentation and examples

This page briefly overviews the APIs that the OER World Map offers its users and the public. It is meant for experienced users and developers to use and build upon the OER World Map's rich data set.
We are curious about your use-cases and the results that the integrations will result in, so please let us know!

If you have any further questions, suggestions or experience any difficulty, reach out to us and the developers at [Gitlab.com](https://gitlab.com/oer-world-map/oerworldmap/-/issues) or via e-mail at [support@oerworldmap.org](mailto:support@oerworldmap.org).

Please use the API responsibly, otherwise the OER World Map team would need to restrict your or the public's access to it.

Also, if you would like to reuse OER World Map data, please contact us at [support@oerworldmap.org](mailto:support@oerworldmap.org).

## The frontend trick

The web application is a JavaScript-based application that runs in your browser. For every action taken by you, it queries the backend for the data. That means you can observe the network requests made by your browser with the browsers internal developer tools (<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>I</kbd>. This method shows all the possible API endpoints and options in real-life scenarios.

## The editor-tool

The [OER World Map Scripts repository](https://gitlab.com/oer-world-map/import-scripts) contains the `editor.py` tool which is used for several maintenance operations.
You can use it as starting point or even as tool to fulfill your needs if it matches your use-cases.

```bash
$ ./editor.py -h
usage: Search and Editor [-h] [-v] [-n] [--step] [-H HOSTNAME] [--elastic-hostname ELASTIC_HOSTNAME] [-C COOKIE] [-i [IDS ...]] [-t TYPE] [-e [EXISTS ...]] [-S] [-I] [--larger-than LARGER_THAN] [-l LIMIT] [--delete-field DELETE_FIELD]
                         [--delete-resource] [--convert-service-available-channel] [--convert-display-name] [--convert-event-alternateName] [--show-links] [--ignore-errors]

Searches data in OER World Map

options:
  -h, --help            show this help message and exit
  -v, --verbose
  -n, --dry-run
  --step
  -H HOSTNAME, --hostname HOSTNAME
  --elastic-hostname ELASTIC_HOSTNAME
  -C COOKIE, --cookie COOKIE
                        Authentication session cookie ("mod_auth_openidc_session=...")
  -i [IDS ...], --ids [IDS ...]
                        Resource IDs to process (instead of search)
  -t TYPE, --type TYPE
  -e [EXISTS ...], --exists [EXISTS ...]
  -S, --show-search     Show the full resource per search result
  -I, --show-ids        Show the resource ID for the search result. Useful for the --exists option without --show-search
  --larger-than LARGER_THAN
                        Only show fields with more than the given list elements. Only works with --exists and --show-search
  -l LIMIT, --limit LIMIT
  --delete-field DELETE_FIELD
  --delete-resource
  --convert-service-available-channel
  --convert-display-name
  --convert-event-alternateName
  --show-links, -L
  --ignore-errors
```

For example, to filter for Events with hashtags and show their URLs along with the hashtag data:
```bash
./editor.py -H https://oerworldmap.org/ -t Event -e about.hashtag --show-links --show-search
```

## Querying individual resources

To download a single resource by ID, you can use the `/resource/$id` endpoint and request JSON data, for example:

```bash
curl -H 'Accept: application/json' https://oerworldmap.org/resource/urn:uuid:29056e88-727b-407e-b00a-16df279ac506
```

## Searching for data

The OER World Map exposes an Elastic Search interface which you can use to query for all kinds of data at `https://oerworldmap.org/elastic/oerworldmap/`.

For example, to query all resources with type Service and return a bunch of fields per result:
```
curl 'https://oerworldmap.org/elastic/oerworldmap/_msearch?' -H 'content-type: application/x-ndjson' -H 'Accept: application/json' --data-raw $'{"preference": "q"}
{"query": {"bool": {"must": [{"bool": {"must": [{"term": {"about.@type": "Service"}}]}}]}},"size": 10,"_source": {"includes": ["about.name.*","about.description.*","about.alternateName.*","about.*.name.*"],"excludes": []}}\n' | jq .
{
  "responses": [
    {
      "took": 1,
      "timed_out": false,
      "_shards": {
        "total": 5,
        "successful": 5,
        "skipped": 0,
        "failed": 0
      },
      "hits": {
        "total": 920,
        "max_score": 2.3143153,
        "hits": [
          {
...
```
With human-readable formatting, the above query looks like:
```json
{
  "preference": "q"
}
{
  "query": {
    "bool": {
      "must": [
        {
          "bool": {
            "must": [
              {
                "term": {
                  "about.@type": "Service"
                }
              }
            ]
          }
        }
      ]
    }
  },
  "size": 10,
  "_source": {
    "includes": [
      "about.name.*",
      "about.description.*",
      "about.alternateName.*",
      "about.*.name.*"
    ],
    "excludes": []
  }
}
```

A great start is, as mentioned above, to look at the browser's queries to `https://oerworldmap.org/elastic/oerworldmap/_msearch?`.

Please refer to the ElasticSearch's documentation on it's [Query DSL (Domain Specific Language)](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html) for all possible options.

Further, please refer to the index configuration file in the repository ([`conf/index-config.json`](https://gitlab.com/oer-world-map/oerworldmap/-/blob/develop/conf/index-config.json?ref_type=heads)) for the description of the index's values and mapping.
The same is also available at `https://oerworldmap.org/elastic/oerworldmap/_mapping`.

## Adding and modifying data

Similarly to retrieving data, the `/resource` endpoint can be used to add or change existing entries.

Before doing any mass-changes, please [get in touch with the OER World Map team](mailto:support@oerworldmap.org).

### Authentication

For any write-operations, you need to be authenticated with your OER World Map user account. If you don't have one, you'll need to create one first. All changes you make, using the API or the web interface, are linked to your user account.

Once you are logged in in the web browser, extract the cookie with the name `mod_auth_openidc_session`. Send this cookie with every request.

### Adding data

To create new resources, make a POST request to `https://oerworldmap.org/resource/` with JSON data, for example:

```bash
curl 'http://worldmap.docker:8080/resource/' \
    -H 'Accept: application/json' \
    -H 'content-type: application/json' \
    -H 'Cookie: mod_auth_openidc_session=YOUR_COOKIE' \
    --data-raw $'{"@type":"Event","name":{"en":"Example event"},"location":[{"address":{"addressCountry":"LA"}}],"inLanguage":["en"],"primarySector":[{"@id":"https://oerworldmap.org/assets/json/sectors.json#general","@type":"Concept","inScheme":{"@id":"https://oerworldmap.org/assets/json/sectors.json"},"name":[{"@language":"en","@value":"Cross-sector"},{"@language":"de","@value":"Sektoren\xfcbergreifend"}],"topConceptOf":{"@id":"https://oerworldmap.org/assets/json/sectors.json"}}],"startDate":"2024-10-30","endDate":"2024-10-30","url":"https://oerworldmap.org/"}'
```

### Modifying existing data

You can make the exact same request as above to `https://oerworldmap.org/resource/urn:uuid:EXISTING_UUID` to change an existing entry.